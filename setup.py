from setuptools import setup, find_packages

DATA_FILES=[
    ('//lib/systemd/system', 
        [
            'linux/systemd/hcaned@.service', 
            'linux/systemd/hcaned-framerd@.service', 
            'linux/systemd/hcaned-mdatad@.service', 
            'linux/systemd/hcaned-ftpd@.service', 
            'linux/systemd/hcaned@.timer',
            'linux/systemd/hcaned-refreshd@.target'
        ] 
    ),
    ('//etc/hcaned', 
        ['linux/conf/hcaned.conf'] 
    )
]
ENTRY_POINTS={
    "console_scripts": [
        "hcaned = hcane.main:bootstrap_main",
        "hcaned-framerd = hcane.main:bootstrap_framer",
        "hcaned-mdatad = hcane.main:bootstrap_mdatad"
    ]
}
setup(
    name='hcaned',
    version="0.1",
    url='https://verse.1337.cx/hcane',
    author='Josef C. Willhite',
    author_email='joeycw328@gmail.com',
    license='MIT',
    description='Satellite hurricane data aggregator and video producer',
    packages=find_packages(),
    package_data={
        "hcane.img":["*.jpg","*.ttf"]
    },
    requires=["pillow","ptvsd", "nose", "setuptools","requests","numpy"],#, "tkinter"],
    data_files=DATA_FILES,
    entry_points=ENTRY_POINTS,
    python_requires='>=2.7',
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
    ]
)
