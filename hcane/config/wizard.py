"""
New storm setup wizard

Contains implementations of various user prompts and associated infrastructure 
to cobble together a setup wizard for creating new storm configurations. The 
main takeaways are the storm name, which goes sector, and information on how to 
process the raw image frames 

@file hcane/config/wizard.py
@author Josef C. Willhite
@date 15 July 2019
"""
import sys
import argparse
import re
import os
from os.path import join, exists
from time import sleep
from operator import add, sub
from PIL import Image
from configparser import ConfigParser 

import hcane
from hcane.imager import imagers
from hcane.client import NESDISCDNClient as Client
from hcane.client import NHCMetadataClient as MDClient

from hcane.img.view import ViewPortFOV, ViewPort
from hcane.img.credit import add_credit, make_credit
from hcane import runtime

from hcane import eyelevel 

daemon_args =runtime.args.daemon

SPLASH              = "Running setup wizard for storm: %s..."
ID_ALERT            = "NHC Storm ID: %s"
NOID_ALERT          = "WARNING:Could not find NHC ID for storm '%s'. EyeLevel imager support will be disabled."
EYELEVEL_ALERT      = "The EyeLevel framer is supported in this sector!"
FLOATER_ALERT       = "Floater imagery is available for this storm!"
FRAMER_ALERT        = "Using framer: %s"
FLOATER_AVAIL       = "Available floaters: "
SECTOR_AVAIL        = "Available GOES sectors: "
RESOLUTION_AVAIL    = "Available imagery resolutions: "
PROMPT_USE_EYELEVEL = "Would you like to use the EyeLevel framer for this storm? [y/n]: "
PROMPT_USE_FLOATER  = "Would you like to use floater imagery for this storm? [y/n]: "
PROMPT_FLOATER_ID   = "Please choose a floater [0-%02d]: "
PROMPT_FLOATER_NAME = "Please choose a name for this floater: "
PROMPT_SECTOR       = "Please choose a sector [0-%02d]: "
PROMPT_RESOLUTION   = "Please choose a resolution [0-%02d]: "
PROMPT_CROP_OK      = "Is the current offset (%+dpx,%+dpx) acceptable? [y/n]: "
PROMPT_ADJUST       = "Enter vertical adjustment [+/- px]: "
PROMPT_HADJUST      = "Enter horizontal adjustment [+/- px]: "
PROMPT_CONFIRM      = "Is this ok? [y/n]: "
PROMPT_RETRY        = "Would you like to retry? [y/n]: "
PROMPT_STARTDATE    = "Would you like to choose a start date? Default: %s (today) [y/n]:  "
PROMPT_DATE         = "Enter year and integral day [YYYYDDD]:  "

class WizardError(Exception):
    """
    Error thrown by the setup wizard
    """

def _prompt_user(prompt_string, validated):
    """
    Prompt the user for input until valid input is entered
    """
    while True:
        user_input=input(prompt_string)
        try: 
            valid_result=validated(user_input.strip())
            if valid_result==None: raise ValueError()
            return valid_result
        except: 
            print("Input invalid!")
            continue

def prompt_sector():
    # print(info for supported sectors)
    # sector_list=sectors.all()
    sector_list=[]
    for key,imager in imagers.all():
        sector_list+=imager.sectors.all()
    print(SECTOR_AVAIL)
    print
    for idx,(name,web_path) in enumerate(sector_list):
        print("  [%02d] %s"%(idx, name))
    print
    # prompt for sector with validator
    sec_cnt=len(sector_list)-1
    _prompt=PROMPT_SECTOR%sec_cnt
    def validator(user_input):
        sector_idx=int(user_input)
        if not (0 <= sector_idx <= sec_cnt): return None
        return sector_idx
    selected_idx=_prompt_user(_prompt, validator)
    # discern sector info from selection
    sec_name, ftp_path=sector_list[selected_idx]
    print
    print("Selected sector: %s"%sec_name)
    return sec_name, ftp_path

def prompt_floater(args):
    # check for a floater
    name=args.name
    floater_ids=Client.get_floater_ids(name)
    if not floater_ids: return False 
    print(FLOATER_ALERT)
    print
    use_floater=_prompt_user(PROMPT_USE_FLOATER, valid_yes_no)=='y'
    if not use_floater: return False
    print
    for idx,(id,name) in enumerate(floater_ids):
        print("  [%02d] %s: %s"%(idx, id, name))
    print
    # prompt for floater with validator
    floater_cnt=len(floater_ids)-1
    _prompt=PROMPT_FLOATER_ID%floater_cnt
    def validator(user_input):
        floater_idx=int(user_input)
        if not (0 <= floater_idx <= floater_cnt): return None
        return floater_idx
    selected_idx=_prompt_user(_prompt, validator)
    # discern floater info from selection
    floater_id, floater_name=floater_ids[selected_idx]
    print
    print("Selected floater with id: %s"%floater_id.upper())
    runtime.args.source.FLOATER=True 
    runtime.args.source.FLOATER_ID=floater_id.upper() 
    # if user entered floater id as the storm name (necessary sometimes when 
    # nesdis doesn't update the floater id txt file), prompt user for a storm
    # name 
    def validator(user_input):
        # name must be nonempty string without whitespace
        if not re.match("^[\w]+$",user_input.strip()): return None
        return user_input.strip()
    if floater_name==None:
        storm_name=_prompt_user(PROMPT_FLOATER_NAME, validator)
        args.name=storm_name.lower()
    return True

def prompt_eyelevel(args):
    # check for nhc storm id 
    if not runtime.args.metadata.ID: return False
    # check if sector is supported
    if not eyelevel.supports_sector(runtime.args.source.SECTOR):
        return False
    print()
    print(EYELEVEL_ALERT)
    # ask the user if they want to use eyelevel
    use_el=_prompt_user(PROMPT_USE_EYELEVEL,valid_yes_no)=='y'
    if not use_el: return False
    runtime.args.image.FRAMER=runtime.args.image.framers.EYELEVEL
    print()
    print("Using EyeLevel framer.")
    return True


def valid_yes_no(user_input):
    choice=user_input.strip().lower()
    if choice in ['y','n']: return choice

def prompt_crop(img_file):
    """
    Prompt the user for crop parameters
    """
    def offset(crop_box):
        reference=(0,875,0,0)
        return tuple(map(sub, reference, crop_box))[1]
    # load the original image using pillow
    OG_IMAGE_FILE='.'.join([img_file,"orig"])
    og_image=Image.open(OG_IMAGE_FILE)
    # create a copy so the original can be archived
    tmp_image=og_image.copy()
    tmp_image.filename=og_image.filename
    # create the field of view from the image metadata 
    view_height=min(2160,tmp_image.height)
    view_width=int(view_height*(16./9.))
    fov=ViewPortFOV(tmp_image.width,tmp_image.height,view_width, view_height)
    crop_window=ViewPort(fov)
    def valid_adjustment(curr_offset, horizontal=False): 
        def validate(usr_input): return int(usr_input) 
        return validate
    # crop at center as initial value
    hoffset_capable=tmp_image.width>view_width
    crop_window.set_center(*fov.center)
    crop_attempt=crop_window.create_view(tmp_image)
    crop_attempt.filename=tmp_image.filename
    credit=make_credit(crop_attempt)
    add_credit(crop_attempt, credit)
    crop_attempt.show()
    while True:
        # ask user if it's acceptable, and return it if so
        prompt=PROMPT_CROP_OK%fov.to_cartesian(*crop_window.center)
        is_ok=_prompt_user(prompt,valid_yes_no)=='y'
        if is_ok: return fov.to_cartesian(*crop_window.center)
        # otherwise, prompt for adjustment and adjust accordingly
        center_x,center_y=crop_window.center
        x_cart, y_cart=fov.to_cartesian(center_x,center_y)        
        if hoffset_capable:
            hadjustment=_prompt_user(PROMPT_HADJUST, valid_adjustment(center_x, True))
            x_cart+=hadjustment
        adjustment=_prompt_user(PROMPT_ADJUST, valid_adjustment(center_y))
        y_cart+=adjustment
        crop_window.set_center(*fov.to_pil(x_cart, y_cart))
        crop_attempt=crop_window.create_view(tmp_image)
        add_credit(crop_attempt, credit)
        # show the new result
        crop_attempt.show()
    
def prompt_start_date():
    """
    Prompt the user for an alternate start date 

    Dates are of the form 'YYYYDDD' where 'YYYY' is the year, and 'DDD' is the 
    integral day of the year. Drawn from the NESDIS image naming scheme
    """
    default_start_date=runtime.args.source.STARTDATE
    _prompt_str=PROMPT_STARTDATE%default_start_date
    def valid_date(date_str):
        if len(date_str)!=7: return 
        if int(date_str) > int(default_start_date): return 
        return date_str
    if _prompt_user(_prompt_str, valid_yes_no)=='y':
        return _prompt_user(PROMPT_DATE, valid_date)
    return default_start_date 

def get_resolutions(): 
    """
    Query the ftp server for supported image resolutions
    """
    def_res=runtime.args.image.SOURCE_RES
    runtime.args.image.SOURCE_RES=""
    # with NESDIS.Client(ftp_path) as client:
        # get full image list
        # all_images=client.get_image_list()
    all_images=Client.get_image_list()
    if not all_images: raise WizardError("Server returned no results.")
    all_images=sorted(all_images)
    # start from the end, and add the resolutions for the last 10 images to a 
    # set 
    discovered_resolutions=set()
    for i in range(10):
        idx=-1*(i+1)
        img=all_images[idx].split('.')[0]
        if any([x in img for x in ["latest","thumbnail"]]): continue
        resolution=img.split('-')[-1]
        discovered_resolutions.add(resolution)
    runtime.args.image.SOURCE_RES=def_res
    sorter=lambda res: int(res.split('x')[0])
    return sorted(list(discovered_resolutions), key=sorter, reverse=True)

def prompt_resolution():
    # print(info for supported sectors)
    print()
    print("Getting available imagery resolutions...")
    print
    res_list=get_resolutions()
    print(RESOLUTION_AVAIL)
    print
    for idx,(resolution) in enumerate(res_list):
        print("  [%02d] %s"%(idx, resolution))
    print
    # prompt for resolution with validator
    res_cnt=len(res_list)-1
    _prompt=PROMPT_RESOLUTION%res_cnt
    def validator(user_input):
        res_idx=int(user_input)
        if not (0 <= res_idx <= res_cnt): return None
        return res_idx
    selected_idx=_prompt_user(_prompt, validator)
    # discern resolution info from selection
    resolution=res_list[selected_idx]
    print
    print("Selected imagery resolution: %s"%resolution)
    return resolution
        
def offset_wizard(args):
    """Prompt user for desired image offset with visual aide"""
    print()
    print("Using legacy framer...")
    runtime.args.image.FRAMER=runtime.args.image.framers.LEGACY
    # get latest imagery
    latest_idx=-1
    if args.rewind: 
        print("Rewinding imagery %d hours!"%args.rewind)
        latest_idx=-1*(12*args.rewind)
    print("Getting latest imagery...")
    sys.stdout.flush()
    all_images=[]
    IMAGE_FILE=None
    # get full image list, throw if it's empty
    all_images=Client.get_image_list(filter_resolution=True)
    if not all_images: raise WizardError("Server returned no results.")
    # if the user wants an earlier image, get it 
    IMAGE_FILE=all_images[latest_idx]
    OG_IMAGE_FILE='.'.join([IMAGE_FILE, "orig"])
    with open(OG_IMAGE_FILE, "wb+") as jpeg_file:
        jpeg_file.write(Client.get_image(IMAGE_FILE))

    print("Starting offset wizard for storm: %s..."%args.name)
    print
    img=runtime.args.image
    img.HOFFSET,img.OFFSET=prompt_crop(IMAGE_FILE)
    if exists(OG_IMAGE_FILE): os.remove(OG_IMAGE_FILE)
    print("Offset wizard completed.")


    
def _run(args):
    """
    Prompt the user for hcaned storm configuration 

    Initiate a series of console prompts that allow the user to input 
    configuration parameters describing a new storm for hcaned to track.
    """
    md=runtime.args.metadata
    source=runtime.args.source
    print(SPLASH%args.name)
    storm_id=MDClient.get_storm_id(args.name)
    if not storm_id: print(NOID_ALERT%args.name)
    else: print(ID_ALERT%(storm_id))
    md.NAME=args.name
    md.ID=storm_id
    # check if there is a floater for this storm that should be used 
    # using_floater=prompt_floater(args)
    # if not using_floater:
    # prompt user for GOES sector
    sector, ftp_path=prompt_sector()
    runtime.args.source.SECTOR=sector
    # set an imagery resolution 
    try:
        # resolution=prompt_resolution(ftp_path)
        resolution=prompt_resolution()
        runtime.args.image.SOURCE_RES=resolution
        # use eyelevel framer if supported/desired
        if not prompt_eyelevel(args):
            offset_wizard(args)
    # # get latest imagery
    # latest_idx=-1
    # print
    # if args.rewind: 
    #     print("Rewinding imagery %d hours!"%args.rewind)
    #     latest_idx=-1*(12*args.rewind)
    # print("Getting latest imagery...")
    # sys.stdout.flush()
    # all_images=[]
    # IMAGE_FILE=None
    # # get full image list, throw if it's empty
    # all_images=Client.get_image_list(filter_resolution=True)
    # if not all_images: raise WizardError("Server returned no results.")
    # # if the user wants an earlier image, get it 
    # IMAGE_FILE=all_images[latest_idx]
    # OG_IMAGE_FILE='.'.join([IMAGE_FILE, "orig"])
    # with open(OG_IMAGE_FILE, "wb+") as jpeg_file:
    #     jpeg_file.write(Client.get_image(IMAGE_FILE))
    except KeyboardInterrupt:
        raise
    except:
        print("Error details: ")
        import traceback as tb 
        for line in tb.format_exc().strip().split('\n'):
            print("> %s"%line )
        return False 
    # print()
    # print("Starting offset wizard for storm: %s"%args.name)
    # hoffset,offset=prompt_crop(IMAGE_FILE)
    source.STARTDATE=prompt_start_date()
    print()
    print("New storm configuration: ")
    print()
    name=md.NAME if not md.ID else "%s [%s]"%(md.NAME,md.ID)
    print("  Name:       %s"%name)
    if source.FLOATER:    
        print("  Floater:    %s"%source.FLOATER_ID )
    else:
        print("  Sector:     %s"%source.SECTOR )
    img=runtime.args.image
    print("  Resoluton:  %s"%img.SOURCE_RES )
    if img.FRAMER==img.framers.LEGACY:
        print("  Offset:     %+d"%img.OFFSET )
        print("  H-Offset:   %+d"%img.HOFFSET )
    else:
        print("  Framer:     %s"%img.FRAMER )
    print("  Start Date: %s"%source.STARTDATE )
    print()
    response=_prompt_user(PROMPT_CONFIRM, valid_yes_no)
    if response=='n': return False 
    print()
    cfg_file='.'.join([md.NAME,"ini"])
    # cfg_path=join(daemon_args.STORMDIR, cfg_file)
    new_config=join('.', cfg_file)
    parser=ConfigParser()
    parser.optionxform=str
    # add sections for manually configurable stuff
    parser.add_section("metadata")
    parser.add_section("image")
    parser.add_section("source")
    # add metadata
    parser.set("metadata", "NAME", md.NAME)
    if md.ID: parser.set("metadata", "ID", md.ID)
    # add source/imager configuration
    if source.FLOATER:    
        parser.set("source", "FLOATER", True)
        parser.set("source", "FLOATER_ID", source.FLOATER_ID)
    else:
        parser.set("source", "SECTOR", source.SECTOR)
    parser.set("source", "STARTDATE", str(source.STARTDATE))
    parser.set("image", "SOURCE_RES", img.SOURCE_RES)
    parser.set("image", "TARGET_VRES", str(img.TARGET_VRES))
    if img.FRAMER==img.framers.LEGACY:
        parser.set("image", "OFFSET", str(int(img.OFFSET)))
        parser.set("image", "HOFFSET", str(int(img.HOFFSET)))
    else:
        parser.set("image", "FRAMER", img.FRAMER)
    # print("Writing storm config to file: %s..."%cfg_path,)
    print("Writing new config '%s' to current directory..."%new_config,)
    sys.stdout.flush()
    with open(new_config, "w+") as storm_config:
        parser.write(storm_config)
    print("success.")
    print
    sleep(2)
    msg="Move your new config to the storm config directory:"
    print(msg )
    print()
    print("  %s"%daemon_args.STORMDIR )
    print()
    print("To start monitoring storm '%s'!"%args.name)
    print()
    sleep(1)
    return True

def run(args):
    # ensure we have sufficient permissions to create storm files 
    if not runtime.init_daemon():
        return False
    # run the wizard in a loop, allowing the user to retry on failure
    while True:
        # run the wizard, exit on success
        if _run(args): break
        # on failure, prompt for retry. If it is denied, end the wizard
        response=_prompt_user(PROMPT_RETRY, valid_yes_no)
        if response=='n': return False
    
    return True


