#!/usr/bin/env python
"""
Entrypoints into the hcaned infrastructure

Contains main methods, command line argument parsing, everything a growing body
needs

@file hcane/main.py
@author Josef C. Willhite
@date 15 July 2019
"""
import os
import argparse 
import logging 
import time
import traceback 
import signal 

from . import hcane 
from . import runtime 
from . import framer
from . import archiver
from . import metadata
from .runtime import args
from .hcane import log
from .config import wizard


DESCRIPTION="""\
hcane is a system service that conjures high-definition video from raw spacecraft 
imagery provided by the GOES-EAST satellite. It does this by scraping raw high-def 
(4000x4000px) image frames using the NESDIS public FTP API, and using a number of 
common linux system utilites (ffmpeg, imagemagick, etc.) to stitch them into a 
video stream. The service can be used by space and extreme-weather enthusiasts 
to create compelling earthscapes for human comsumption.\
"""

EPILOG="""\
Created by Josef C. Willhite, 11 July 2019\
"""

cline=argparse.ArgumentParser("hcane", description=DESCRIPTION, epilog=EPILOG)
cline.add_argument("name", help="Hurricane name (used to resolve to a metadata file")
cline.add_argument("-r", "--rewind-hours", dest="rewind", metavar="H", action="store", type=int, default=0, help="Use imagery from H hours in the past (only works with the --create flag)")
cline.add_argument("-d", "--log-debug", dest="log_debug", action="store_true", help="Output debug logging")
cline.add_argument("-D", "--debug", action="store_true", help="Launch with ptvsd remote debugging enabled")
cline.add_argument("-p", "--debug-port", dest="debug_port", action="store", default=8888, type=int, help="ptvsd remote debugging port")
cline.add_argument("-n", "--new", action="store_true", help="Start the new storm configuration wizard")
cline.add_argument("-a", "--archive", action="store_true", help="Start the archiver")
cline.add_argument("--force-requeue", dest="requeue", action="store_true", help="(Re)add all images to the frame processing queue, not just newly downloaded ones")
cline.add_argument("--reenqueue", dest="queue_only", action="store_true", help="Don't download new images, just add all extant images to the frame processing queue.")

FRAMER_DESCRIPTION="""\
hcaned frame formatter. See hcaned -h for details.\
"""

framer_parser=argparse.ArgumentParser("hcaned-framerd", description=FRAMER_DESCRIPTION, epilog=EPILOG)
framer_parser.add_argument("name", help="Hurricane name (used to resolve to a metadata file")
framer_parser.add_argument("-d", "--log-debug", dest="log_debug", action="store_true", help="Output debug logging")
framer_parser.add_argument("-D", "--debug", action="store_true", help="Launch with ptvsd remote debugging enabled")
framer_parser.add_argument("-p", "--debug-port", dest="debug_port", action="store", default=8888, type=int, help="ptvsd remote debugging port")
framer_parser.add_argument("-r", "--vres", dest="TARGET_VRES", action="store", default=2160, type=int, help="Override vertical resolution of target frames (preserves 16:9 aspect ratio)")
framer_parser.add_argument("-C", "--no-credit", dest="ADD_CREDIT", action="store_false", default=True, help="Do not add credit to output frames")

# default parser is cline parser but can be changed
parser=cline

log=None
def shutdown_sig(signum, frame):
    log.warn("Caught SIGTERM from OS.")
    log.warn("Terminating process pool")
    if hcane.pool !=None:
        hcane.pool.close()
        hcane.pool.terminate()

def main(clargs):
    # init the runtime environment 
    if not runtime.init(clargs): return False
    # hcane.HCANE_NAME=clargs.name
    hcane.HCANE_NAME=args.metadata.NAME
    # if clargs.log_debug: hcane.LOG_LEVEL=logging.DEBUG
    if clargs.log_debug: args.daemon.LOGLEVEL=logging.DEBUG
    # all_results=hcane.scrape_imagery()
    # raise errors that occured in jobs
    log.info("Bootstrapping hcaned for storm: %s"%args.metadata.NAME)
    exception=None
    signal.signal(signal.SIGTERM, shutdown_sig)
    starttime=time.time()
    all_results=hcane.x_scrape_imagery_parallel(args.daemon.DATADIR)
    # handle shutdown signal 
    try:
        for job_result in all_results:
            res=job_result.get()
            if res: 
                CONTINUE=False; exception=res
                raise hcane.JobFailureError()
    except KeyboardInterrupt:
        log.warn("Operation aborted!")
        log.info("Terminating process pool")
        hcane.pool.terminate()
    except hcane.JobFailureError:
        log.error("A job has failed!!")
        log.info("Terminating process pool")
        hcane.pool.terminate()
    except:
        log.error("Unknown error")
        exception=traceback.format_exc()
        log.info("Terminating process pool")
        hcane.pool.terminate()
    # signal.pause()
    log.warn("Waiting for pool shutdown...")
    hcane.pool.join()
    duration=time.time()-starttime
    log.info("Shutdown complete.")
    log_=log.critical if exception else log.info
    if exception:
        log_("--")
        for line in exception.strip().split('\n'):
            log_(line)
        log_("--")
    log_("Got %d images in %.2f seconds"%(hcane.received,duration))
    log.info("Exiting.")
    return bool(exception)



def bootstrap(func, pass_args=False, init_daemon=False, init_storm=False):
    global log
    cline_args=parser.parse_args()
    log=logging.getLogger("hcane.%s.main"%cline_args.name)
    logging.basicConfig()
    level=logging.INFO 
    if cline_args.log_debug: level=logging.DEBUG
    logging.getLogger().setLevel(level)
    # if launched in debug mode, break into ptvsd debugger 
    if cline_args.debug:
        runtime.enable_debug(cline_args.debug_port)
    # enter main routine
    try:
        # try to do config init if directed as such 
        if init_storm and not runtime.init(cline_args): return False
        elif init_daemon and not runtime.init_daemon(): return False
        # main routine for new storm wizard
        if pass_args: return int(func(cline_args))
        else: return int(func())
    except Exception as e:
        log.error("Unclean shutdown")
        err=traceback.format_exc()
        for line in err.strip().split('\n'):
            log.critical(line)
        return 1
    except KeyboardInterrupt:
        print
        print("Aborted.")
    except:
        print("Unknown Condition")
        import traceback as tb 
        for line in tb.format_exc().strip().split('\n'):
            print("> %s"%line)
        return 1

def bootstrap_main():
    cline_args=cline.parse_args()
    # if cline_args.create: return bootstrap(wizard.run, pass_args=True)
    if cline_args.new: 
        return bootstrap(wizard.run, pass_args=True)
    elif cline_args.archive: 
        return bootstrap(lambda: archiver.run(args), init_storm=True)
    else: 
        return bootstrap(main, pass_args=True)

def bootstrap_framer():
    global parser
    parser=framer_parser
    return bootstrap(lambda: framer.run(args), init_storm=True)

def bootstrap_mdatad():
    return bootstrap(metadata.run, init_storm=True)

if __name__=="__main__":
    bootstrap()