import os
from os.path import join 

class FramerIf(object):
    """
    Unified framer interface
    """
    def __init__(self, queue_dir, frame_dir):
        """
        Placeholder for framer environment setup
        """
        self.__queue_dir=queue_dir
        self.__frame_dir=frame_dir

    @property 
    def queue_dir(self): return self.__queue_dir
    
    @property 
    def frame_dir(self): return self.__frame_dir
    
    def resolved_path(self, queued_filename):
        """
        Return the real abspath (with resolved symlink) of the inputed 
        jpg in the framer queue. 
        """
        # resolve symlink and return
        return os.readlink(self.enqueued_path(queued_filename))
    
    def enqueued_path(self, queued_filename):
        """
        Return full path of enqueued file
        """
        return join(self.__queue_dir, queued_filename)  

    def prepare_list(self, image_list):
        """
        Perform any preparations to the image list before submitting it as a job 
        list. Should return a list of items suitable for passing to the dispatch 
        method.
        """

    def dispatch(self, pil_img, *args):
        """
        Perform the framing operation on the image, and return the processed 
        frame
        """
    