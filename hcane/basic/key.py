"""
Base classes for hcane key management utilities

This package holds abstract interfaces that support easy key management

@file hcane/basic/key.py
@author Josef C. Willhite
@date 13 July 2019
"""

from collections import OrderedDict

class Key(object):
    def __init__(self, key): self.key=key

class KeyContainer(type):
    """
    Container metaclass for supporting quick/easy key retrieval

    This interface provides the capability to add named keys to a class and have
    them be easily retrievable as a list
    """
    

    def __new__(mcs, name, bases, init_dict):
        """
        Override to provide key management 

        """
        all_keys=OrderedDict()
        for attr, val in init_dict.items():
            if isinstance(val, Key):  stored_value=val.key 
            elif isinstance(val, KeyContainer): stored_value=val
            else: continue
            all_keys[attr]=stored_value 
            init_dict[attr]=stored_value
        cls=type.__new__(mcs, name, bases,init_dict)
        cls.__all_keys=all_keys
        return cls
    
    def get(cls, key):
        """
        Return the value for the inputed string
        """
        try:
            # try to get key by id
            return getattr(cls, key)
        except:
            # try to get key by value
            for id_,value in cls.all():
                if key==value: return key 
            else:
                raise
    
    def all(cls):
        """
        Get all keys known to this class 

        Return the list of all keys that were add to this class via the @ref Key 
        function 
        @return List of keys (as strings) that were added with the Key function
        """
        return [(k,v) for (k,v) in cls.__all_keys.items()]


class KeyManager(object, metaclass=KeyContainer):
    """Key management"""