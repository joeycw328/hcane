#!/usr/bin/env python
"""
Base classes for hcane infrastructure

This package holds abstract base classes, metaclasses, and other basic 
implementations that provide functionality to the hcane system

@file hcane/basic/__init__.py
@author Josef C. Willhite
@date 13 July 2019
"""
from . import key