from hcane import eyelevel
from hcane.img.view import ViewPortFOV, ViewPort

params=eyelevel.params

class EyeViewPort(ViewPort):
    """
    Facility for providing a size-appropriate view port into 
    a large PIL image centered around the eye of a storm.
    """
    def __init__(self, fov):
        """Init base classes"""
        ViewPort.__init__(self, fov)

    def set_center(self, lat, lon, apply_geodesic=True):
        """
        Update the longitude and lattitude of the storm's eye, and thereby the 
        location of the viewport. This function takes into consideration the 
        bounds of the image and the bounds of the viewport, and stores the updated
        viewport properties (read: x,y offsets) such that the viewport remains
        within the bounds of the img_width and img_height passed to the 
        constructor
        """
        # move viewport center to updated coordinates 
        x,y=lat,lon
        ViewPort.set_center(self, x, y)
        # return x,y in pil (?) coordinates 
        return x,y