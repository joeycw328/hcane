# import requests 
import csv 
import datetime
import time
import os
import re
from os.path import exists,join,dirname
from contextlib import contextmanager
import logging 

from hcane.runtime import args

_MDFILE=".eyeinfo"

_COLUMNS=["timestamp","latitude","longitude"]

TIME_FORMAT="%H%M %Z %a %b %d %Y"
INTEGER_FORMAT="%Y%j%H%M"
LOC_RE_TXT="CENTER LOCATED NEAR ([0-9]+\.[0-9][NS] +[0-9]+\.[0-9][EW])"
LOC_RE=re.compile(LOC_RE_TXT)

FORMATS={
    "forecast": ("%H%M %Z %a %b %d %Y",re.compile("CENTER LOCATED NEAR ([0-9]+\.[0-9][NS] +[0-9]+\.[0-9][EW])")),
    "public":   ("%I%M %p %z %a %b %d %Y",re.compile("LOCATION\.\.\.([0-9]+\.[0-9][NS] +[0-9]+\.[0-9][EW])"))
}

def parse_coords(lat, lon):
    """Convert from N/S, E/W to +/-"""
    is_negative=lat[-1]=='S'
    lat_=float(lat[:-1])*(1-2*is_negative)
    is_negative=lon[-1]=='W'
    lon_=float(lon[:-1])*(1-2*is_negative)
    return lat_,lon_

def parse_advisory(advisory_lines, adv_type="forecast"):
    date=lat=lon=None
    log=logging.getLogger("eyeinfo.adv.parser")
    # fast forward to the date line and extract it
    lines=(line for line in advisory_lines)
    # line=lines.next().strip()
    line=next(lines).strip()
    timefmt,center_re=FORMATS[adv_type]
    while True: 
        try:
            # compensate for EDT/EST
            iline=line.replace("EDT","-0500").replace("EST","-0500")
            iline=iline.replace("CDT","-0600").replace("CST","-0600")
            # compensate for non-zero-padded date
            try:
                if int(iline[:4])<1000 and iline[0] != "0": iline="0"+iline
            except:
                pass
            # dt=datetime.datetime.strptime(line,TIME_FORMAT)
            dt=datetime.datetime.strptime(iline,timefmt)
            # convert to utc
            dt=datetime.datetime.fromtimestamp(time.mktime(dt.utctimetuple()))
            date=dt.strftime(INTEGER_FORMAT)
            break 
        except:
            try: line=next(lines).strip()
            except: break
    # fast forward to the date coords line and extract it
    try: line=next(lines).strip()
    except StopIteration:
        log.warning("Skipping advisory with no date data.")
    while True: 
        # match=LOC_RE.search(line)
        match=center_re.search(line)
        if match:
            res=match.groups()[0].split()
            lat,lon=parse_coords(*res)
            break 
        else:
            try: line=next(lines).strip()
            except: break
    return date,lat,lon


class MalformedMetadataError(Exception):
    """
    Subclass for malformed metadata files 
    """

@contextmanager
def metadata_reader():
    """
    Context manager to aid in reading from metadata file
    """
    name=args.metadata.NAME 
    data_dir=args.daemon.DATADIR
    mdfile=join(data_dir,name,_MDFILE)
    exception=None
    with open(mdfile, "r") as _mdfile:
        try:
            reader=csv.reader(_mdfile)
            # if reader.next() !=_COLUMNS:
            if next(reader) !=_COLUMNS:
                raise MalformedMetadataError()
            yield reader
        except MalformedMetadataError as e:
            print("Malformed metadata file.")
            exception=e
        except Exception as e:
            print("Error occurred during metadata read.")
            exception=e
    if exception!=None: raise exception 

@contextmanager
def metadata_writer():
    """
    Context manager to aid in dumping to metadata file
    """
    # get the path to the metadata file
    name=args.metadata.NAME 
    data_dir=args.daemon.DATADIR
    mdfile=join(data_dir,name,_MDFILE)
    exception=None
    # check for existence, use reader to check well-formed-ness
    file_exists=exists(mdfile)
    if file_exists: 
        with metadata_reader() as reader: pass 
    # write data
    with open(mdfile, "a+b") as _mdfile:
        try:
            # ensure file ends on newline
            if file_exists:
                _mdfile.seek(-1,os.SEEK_END)
                if _mdfile.read().decode('utf-8') !='\n': 
                    _mdfile.write('\n'.encode("utf-8")); _mdfile.flush()

        except Exception as e:
            print("Unknown error occurred during metadata write.")
            exception=e
    with open(mdfile, "a+") as _mdfile:
        try:
            writer=csv.writer(_mdfile)
            if not file_exists: writer.writerow(_COLUMNS)
            yield writer
        except Exception as e:
            print("Unknown error occurred during metadata write.")
            exception=e
    if exception!=None: raise exception 


def write_state_data(*eyestates):
    """
    Write new eyestate to metadata file using default facilities
    """
    log=logging.getLogger("hcane.%s.md.writer"%args.metadata.NAME)
    # type check 
    for eyestate in eyestates:
        if not isinstance(eyestate, EyeState):
            msg="Expected parameter with type %s, got %s"
            msg=msg%(EyeState.__name__, type(eyestate).__name__)
            raise ValueError(msg)
    # get states, to watch for overwrites
    try: latest=load_state_data()
    except IOError: latest=[]
    if latest: latest=latest[-1]
    # get the default metadata writer 
    added=0
    with metadata_writer() as writer:
        # write (only new) eyestates to file
        for eyestate in eyestates:
            if latest and eyestate.datetime <= latest.datetime:
                log.warning("Skipping old eyestate data for time: %s"%eyestate.datetime)
                continue
            writer.writerow(eyestate.dump())
            added+=1
    if added: log.info("Added new eye coordinates at %d timestamps."%added)
    else: log.info("No new eye coordinates added.")

def load_state_data():
    """
    Read metadata file using default facilities and return list of EyeState objects
    """
    states=[]
    # get the default metadata reader 
    with metadata_reader() as reader:
        # create the eye states
        for row in reader:
            states.append(EyeState(*row))
    return states

class EyeState(object):
    DATEFORMAT="%Y%j%H%M"
    def __init__(self,dt_str,lat,lon):
        self.__datetime=datetime.datetime.strptime(dt_str, self.DATEFORMAT)
        self.__int=int(dt_str)
        self.__lat=float(lat)
        self.__lon=float(lon)
    @property 
    def datetime(self): return self.__datetime 
    @property 
    def int(self): return self.__int 
    @property 
    def lat(self): return self.__lat 
    @property 
    def lon(self): return self.__lon 
    
    def dump(self):
        """
        Return eyestate as list
        """
        return [self.int, self.lat, self.lon]
    
    def dumps(self):
        """
        Return eyestate as list
        """
        return [str(i) for i in self.dump()]
 
    @classmethod 
    def from_iterable(cls, iterable=[]):
        """
        Create EyeState from iterable of strings
        """
        return cls(*iterable)
