import csv 
import datetime 
from os.path import join
from contextlib import contextmanager
import logging 

# from hcane.map.transform import BiasedGeodesicTransformation
from hcane.map import transform
from hcane.map.transform import BiasedTransformation
from hcane.runtime import args
from . import eyeinfo
from .eyeinfo import EyeState

class Interval(object):
    """
    A time interval, tracking where the storm eye is at begin and end of interval
    """
    def __init__(self, start_str, end_str, start_coords=(0.0,0.0), end_coords=(0.0,0.0)):
        """
        Store start and end as eyestates
        """
        # store the date format string hint
        self.__start                = EyeState(start_str, *start_coords)
        self.__end                  = EyeState(end_str, *end_coords)

    @property 
    def start(self): return self.__start 
    @property 
    def end(self): return self.__end 

    def coords_from_string(self, datetime_string, strformat=EyeState.DATEFORMAT):
        """
        Return lat,lon at datetime_string extrapolated from stored data
        """
        # turn dt string into datetime object 
        format_=strformat
        desired_date=datetime.datetime.strptime(datetime_string, format_)
        # perform bounds checking 
        if desired_date > self.end.datetime: 
            raise ValueError("Date is after end of current interval")
        if desired_date < self.start.datetime: 
            raise ValueError("Date is before beginning of current interval")
        # calculate elapsed fraction of interval 
        # des_delta=(desired_date-self.start.datetime).total_seconds() 
        # interval_delta=(self.end.datetime-self.start.datetime).total_seconds() 
        # dt_frac=des_delta/interval_delta
        dt_frac=self.datetime_fraction(
            self.start.datetime, desired_date, self.end.datetime 
        )
        # calculate & return lat/lon 
        ret_lat=self.start.lat+(dt_frac*(self.end.lat-self.start.lat))
        ret_lon=self.start.lon+(dt_frac*(self.end.lon-self.start.lon))
        return ret_lat,ret_lon

    @classmethod 
    def datetime_fraction(cls, dt_start, dt_point, dt_end, strformat=EyeState.DATEFORMAT):
        """
        Calculate the fractional elapsation of the point in the interval
        """
        # turn desired dt string into datetime object 
        format_=strformat
        if type(dt_start) in [str,int]:
            dt_point=datetime.datetime.strptime(str(dt_point[:11]), format_)
        # turn interval start/end into dt if it's a string or int 
        if type(dt_start) in [str,int]:
            dt_start=datetime.datetime.strptime(str(dt_start[:11]), format_)
        if type(dt_end) in [str,int]:
            dt_end=datetime.datetime.strptime(str(dt_end[:11]), format_)
        # calculate elapsed fraction of interval 
        des_delta=(dt_point-dt_start).total_seconds() 
        interval_delta=(dt_end-dt_start).total_seconds() 
        return des_delta/interval_delta

        
    @classmethod 
    def from_state_data(cls, start, end):
        """
        Create an interval from two EyeState instances
        """
        # type check 
        if not isinstance(start, EyeState):
            msg="Expected parameter 'start' with type %s, got %s"
            msg=msg%(EyeState.__name__, type(start).__name__)
            raise ValueError(msg)
        if not isinstance(end, EyeState):
            msg="Expected parameter 'end' with type %s, got %s"
            msg=msg%(EyeState.__name__, type(end).__name__)
            raise ValueError(msg)
        # create empty interval 
        fake_date_str=datetime.datetime.now().strftime(EyeState.DATEFORMAT)
        new_int=cls(fake_date_str, fake_date_str)
        # update it's internal start and end state and return
        new_int.__start=start 
        new_int.__end=end 
        return new_int


# class EyeFinder(BiasedGeodesicTransformation):
class EyeFinder(BiasedTransformation):
    """
    Facility for finding the eye of a hurricane (lat/lon) at a given date/time
    """
    def __init__(self, fov, linear=False):
        """
        Store storm name, placeholders for bins
        """
        tform_type=transform.TF_LINEAR if linear else transform.TF_GEODESIC 
        BiasedTransformation.__init__(self, fov, tform_type)
        self.__intervals=[]
        self.log                    = logging.getLogger("hcane.%s.eyefinder"%args.metadata.NAME)

    @property 
    def bins(self): return self.__intervals

    def load_data(self):
        """
        Load interval data from csv
        """
        states=eyeinfo.load_state_data()
        for i in range(len(states)-1):
            self.__intervals.append(
                Interval.from_state_data(states[i],states[i+1])
            )

    def __coords_approx(self, earliest_dt, dt_point, nearby_interval):
        """
        Approximate coordinates for early dates where no data exists 
        """
        # get nearby interval, fraction of datetime between first sample and 
        # current sample
        interval=nearby_interval 
        dt_frac=Interval.datetime_fraction(
            earliest_dt, dt_point, interval.start.datetime
        )
        # get first coordinates with eye data (cartesian)
        lat,lon=interval.start.lat, interval.start.lon 
        x,y=BiasedTransformation.apply(self, lat,lon)
        # approximate new coordinates
        origin=self.fov.x_bias,self.fov.y_bias 
        self.fov.set(origin=self.fov.center)
        x_offset,y_offset=self.fov.to_pil(args.image.HOFFSET,args.image.OFFSET)
        self.fov.set(origin=origin)
        approx_x=x_offset+(dt_frac*(x-x_offset))
        approx_y=y_offset+(dt_frac*(y-y_offset))
        return approx_x, approx_y


    def iter_coordinates(self, datetime_strings, apply_geodesic=False):
        """
        Return a generator that iterates over dt_strings and generates 
        tuples (string, (lat,lon))
        """
        dt_idx=0;early_dts=0
        dt_strings=sorted(datetime_strings)
        earliest_dt=datetime_strings[0]
        for interval in self.bins:
            while dt_idx<len(dt_strings):
                # parse out just datetime portion from full string
                full_string=str(dt_strings[dt_idx])
                dt_only=full_string[:11]
                # if the dt is too late, go on to the next interval 
                if int(dt_only)>interval.end.int:
                    break
                # if the dt is too early, go on to the next dt 
                if int(dt_only)<interval.start.int:
                    msg="Input %s is before interval start. Skipping."
                    self.log.debug(msg%full_string)
                    dt_idx+=1; 
                    early_dts+=1
                    continue 
                # if the dt is in this bin, return it's full string, plus coords 
                dt_idx+=1
                (coord1,coord2)=interval.coords_from_string(dt_only)
                if apply_geodesic:
                    (coord1,coord2)=BiasedTransformation.apply(self, coord1,coord2)
                yield full_string, (coord1,coord2)
        # if there are dt's that are too early for all intervals, output helpful
        # message
        if early_dts:
            msg="%d image(s) were captured before advisories began and will be skipped."
            self.log.warning(msg%early_dts)
        # if there are dt's that are too late for all intervals, output helpful
        # message
        remaining_dts=len(dt_strings)-dt_idx 
        if remaining_dts:
            msg="%d image(s) are deferred pending further forecast advisory data (no eye data)."
            self.log.info(msg%remaining_dts)