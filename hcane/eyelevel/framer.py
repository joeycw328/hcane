from os.path import join, basename, exists
import logging 

from hcane.runtime import args 
from hcane.basic.framer import FramerIf

from . import view
from .view import EyeViewPort, ViewPortFOV
from .finder import EyeFinder

from hcane.imager import imagers

class EyeLevelFramer(FramerIf):
    """
    EyeLevel framer implementation. Provides EyeViewPort, and preprocesses image 
    lists to couple lat/lon parameters
    """
    def __init__(self, queue_dir, frame_dir):
        """
        Create viewport and basic fov
        """
       # superclass init 
        super(EyeLevelFramer, self).__init__(queue_dir,frame_dir)
        self.log=logging.getLogger("hcane.%s.framer.eyelevel"%args.metadata.NAME)
        self.log.info("Initializing EyeLevel frame processing")
        self.log.info("Sector: %s"%args.source.SECTOR)
        # create field of view parameters
        raw_width,raw_height=args.image.SOURCE_RES
        tgt_width,tgt_height=args.image.TARGET_RES
        view_height=min(tgt_height,raw_height)
        view_width=int(view_height*(16./9.))
        fov=ViewPortFOV(raw_width,raw_height,view_width, view_height)
        # update geodesic parameters
        GOES16=imagers.GOES16
        GOES17=imagers.GOES17
        try: sector=GOES16.sectors.get(args.source.SECTOR)
        except: 
            try: sector=GOES17.sectors.get(args.source.SECTOR)
            except: 
                msg="Sector %s does not support EyeLevel."%args.source.SECTOR
                raise ValueError(msg)
        tparams=view.params[sector]
        fov.set(origin=tparams.origin)
        fov.set(scale=tparams.scale)
        fov.set(meridian=tparams.orbital_slot)
        if tparams.linear:
            fov.set(oblation=tparams.y_ratio)
            fov.set(max_degree=tparams.fov_deg)
        # create viewport
        self.viewport=EyeViewPort(fov)
        # create eye finder 
        self.eyefinder=EyeFinder(fov,tparams.linear)
        self.eyefinder.load_data()
    
    @property  
    def final_width(self): return self.viewport.fov.view.width 

    @property  
    def final_height(self): return self.viewport.fov.view.height 
    
    def prepare_list(self, queued_frames):
        """
        Get datetime from original images, and use them to resolve lat/lon from 
        eyefinder
        """
        # generate resolved filenames 
        queue_map={}
        real_frames=[]
        results=[]
        for qframe in queued_frames:
            real_frames.append(basename(self.resolved_path(qframe)))
            queue_map[real_frames[-1]]=qframe
        coords=self.eyefinder.iter_coordinates(real_frames, apply_geodesic=True) 
        for coord in coords:
            # real_frame, (lat,lon)=coord
            real_frame, (x,y)=coord
            results.append((queue_map[real_frame],x,y))
        return results
        # return [(image,) for image in image_list]

    # def dispatch(self, pil_image, (lat,lon)):
    def dispatch(self, pil_image, xy):
        """
        Set center and return cropped image
        """
        x,y=xy
        # self.viewport.set_center(lat, lon)
        self.viewport.set_center(x, y, apply_geodesic=False)
        return self.viewport.create_view(pil_image)
