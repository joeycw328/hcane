from hcane.imager import imagers

# for compatibility
from . import eyeinfo as state

__sector16=imagers.GOES16.sectors
__sector17=imagers.GOES17.sectors

# store geodesic transformation params
class __params(object):
    """
    Image transformation parameters.
    """
    def __init__(self,origin=(0,0),scale=1.0,slot=-75,linear=False,yratio=1.0,fov_deg=60):
        self.__origin=origin 
        self.__scale=scale 
        # slot defaults to 75W, which is GOES16's orbital position
        self.__slot=slot 
        self.__linear=linear 
        self.__yratio=yratio 
        self.__fov_deg=fov_deg 
    @property 
    def origin(self): return self.__origin 
    @property 
    def scale(self): return self.__scale 
    @property 
    def orbital_slot(self): return self.__slot
    @property 
    def y_ratio(self): return self.__yratio
    @property 
    def linear(self): return self.__linear
    @property 
    def fov_deg(self): return self.__fov_deg

# eyelevel geodesic params
params={
    __sector16.TROPICAL_ATLANTIC:       __params(origin=(1824,3870), scale=5444),
    __sector16.US_EAST_COAST:           __params(origin=(1820,9040), scale=10888),
    __sector16.GULF_OF_MEXICO:          __params(origin=(4696,7350), scale=10888),
    __sector16.CENTRAL_AMERICA:         __params(origin=(4096,5318), scale=10888),
    __sector16.CONUS:                   __params(origin=(3612,4650), scale=5444),
    __sector16.EASTERN_EAST_PACIFIC:    __params(origin=(7152,3264), scale=6672, linear=True),
    # Note: sector 17 is GOES 17, which occupies a more westerly orbital slot!
    __sector17.TROPICAL_PACIFIC:        __params(origin=(3824,3824), scale=5444, slot=-137.2)
}

# check for sector support
def supports_sector(sector):
    try: return __sector16.get(sector) in params
    except AttributeError: pass 
    try: return __sector17.get(sector) in params
    except AttributeError: pass 
    return False    