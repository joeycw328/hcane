"""
FTP Client implementation

Contains an FTP client specialized for ripping 2160p (4K) imagery from the 
[NESDIS](https://www.star.nesdis.noaa.gov/GOES/index.php) GOES-EAST FTP server

@file hcane/client.py
@author Josef C. Willhite
@date 13 July 2019
"""
from ftplib import FTP
from contextlib import contextmanager
import datetime 
from os.path import join 
import requests 
import re

from .runtime import args
from .imager import imagers, bands

ConnectionError=requests.exceptions.ConnectionError 

NESDIS_SERVER       = "ftp.star.nesdis.noaa.gov"
TARGET_FTP_DIR	    = "pub/star/goes/GOES16/ABI/SECTOR/gm/GEOCOLOR/"
IMG_EXT             = ".jpg"

IMAGE_RE="([0-9]{11}.*%s.jpg)\""
FSTADV_RE="%s.fstadv.[0-9]{3}"

class NESDISClient(object):
    def __init__(self, target_dir=TARGET_FTP_DIR):
        self.__cache=""
        self.__client=None
        self.__target_dir=target_dir
        self.connect()
        
    def onDataReceived(self, data):
        self.__cache+=data  

    def get_image(self, img_file):
        """
        Get full binary of image from server 
        @return Binary string containing raw jpeg data
        """
        tmp=""
        self.__client.retrbinary("RETR %s"%img_file, self.onDataReceived)
        tmp=self.__cache; self.__cache=""
        return tmp

    def get_image_list(self, days=10):
        """
        Return the list of all 2160p (4K) images in the target dir
        """
        today=datetime.datetime.today()
        day_integral=(today-datetime.datetime(today.year,1,1)).days+1
        IMG_RESOLUTION=args.image.SOURCE_RES
        LATEST_LINK=IMG_RESOLUTION+IMG_EXT
        # query="*"+IMG_RESOLUTION+IMG_EXT
        # full_list=sorted(self.__client.nlst(query))
        full_list=set()
        for i in range(days):
            datestr=str(today.year)+str(day_integral-i)
            query=datestr+"*"+IMG_RESOLUTION+IMG_EXT
            try:
                full_list|=set(self.__client.nlst(query))
            except:
                print("WARN: Query %s returned no results"%query)
        full_list=sorted(full_list)
        if LATEST_LINK in full_list: full_list.remove(LATEST_LINK) 
        return full_list

    def connect(self):
        """
        (Re)connect to nesdis server
        """
        self.__cache=""
        self.__client=FTP(NESDIS_SERVER)
        self.__client.login()
        self.__client.cwd(self.__target_dir)

    @classmethod 
    @contextmanager
    def Client(cls, target_dir=TARGET_FTP_DIR):
        """
        Factory contextmanager for use with the @e with keyword
        """
        tmp_client=cls(target_dir)
        yield tmp_client 
        tmp_client.__client.quit()

class NHCMetadataClient(object):
    """
    Http Client for accessing storm metadata from the NHC public ftp server
    """
    URL_BASE                 = "https://ftp.nhc.noaa.gov/atcf"
    __FORECAST_ADV_BASE       = join(URL_BASE, "mar")
    __ADV_TYPES               = {
        "forecast":     (join(URL_BASE, "mar"), "%s.fstadv.[0-9]{3}"),
        "public":     (join(URL_BASE, "pub"), "%s.public_*\w*\.[0-9]{3}"),

    }
    __STORM_LIST              = join(URL_BASE, "index/storm_list.txt")
    
    @classmethod
    def __get_url(cls, url):
        """
        Get resource at url
        """
        resp=requests.get(url)
        if resp.status_code==404:
            raise ValueError("Resource %s not found on server"%url)
        return resp 

    @classmethod
    def get_advisory(cls, adv_id, adv_type="forecast"):
        """
        Get raw text of the forecast advisory adv_id

        adv_id is usually something like ep082020.fstadv.001 or al122019.fstadv.015
        depending on the basin, storm enumeration, year, and advisory enumeration 
        @return Binary string containing raw forecast advisory text
        """
        
        # base_url=cls.__FORECAST_ADV_BASE
        (base_url,ign)=cls.__ADV_TYPES[adv_type]
        # get the image, be mindfule of nonexistant resources
        adv_url=join(base_url,adv_id) 
        # return binary image 
        return cls.__get_url(adv_url).content

    @classmethod
    def __get_storm_ids(cls):
        """
        Return a dict mapping storm names (e.g. douglas) to NHC storm codes (e.g.
        EP082020)
        """
        # get the storm list as text (note: make sure url has trailing backslash)
        raw_list=cls.__get_url(cls.__STORM_LIST).text
        resp_lines=[str(i.strip()).split(',') for i in raw_list.split('\n')]

        # print("Got %d storms"%len(resp_lines))
        # extract storm names (all lowercase) and ids 
        idmap={}
        for line in resp_lines:
            # print("line len: "+str(len(line)))
            if len(line) < 21: continue
            idmap[line[0].strip().lower()]=line[-1].strip()
        # print("Found %d files in response"%len(files))
        return idmap

    @classmethod
    def get_storm_id(cls,storm_name):
        """
        Get the storm ID for the named storm, or None if none can be found. Storm
        name is case insensitive
        """
        # try to return the storm name
        try: return cls.__get_storm_ids()[storm_name.lower()]
        # return none if it doesn't exist!
        except KeyError: return None

    @classmethod
    def list_advisories(cls, storm_id):
        """
        Return a list of all advisories matching the NHC storm ID storm_id

        storm_id should be something like EP082020 or AL132019 depending on the
        basin, storm enumeration, and year. Return value will be a list of tuples
        (type,advisory) where type is one of the __ADV_TYPES, and advisory is the 
        ftp filename
        """
        # try: IMG_RESOLUTION="%dx%d"%args.image.SOURCE_RES
        # except: IMG_RESOLUTION=args.image.SOURCE_RES
        # if filter_resolution:
        #     image_re=IMAGE_RE%IMG_RESOLUTION
        # else:
        #     image_re=IMAGE_RE%""
        rval=[]
        for (adv_type,(adv_base,adv_re)) in cls.__ADV_TYPES.items():
        # adv_base,adv_re=cls.__ADV_TYPES[adv_type]
        # adv_re=FSTADV_RE%(storm_id.lower())
            adv_re=adv_re%(storm_id.lower())
        # check if we're getting floater imagery or sector imagery
        # source=args.source
        # if source.FLOATER:
        #     base_url=cls.floater_data_url(source.FLOATER_ID, source.BAND)
        # else:
        #     base_url=cls.sector_data_url(source.SECTOR, source.BAND)
        # get the image list as html (note: make sure url has trailing backslash)
        # base_url=join(cls.__FORECAST_ADV_BASE,"")
            base_url=join(adv_base,"")
            raw_response=cls.__get_url(base_url).text
            resp_lines=[str(i.strip()) for i in raw_response.split('\n')]
            # print("Got %d response lines"%len(resp_lines))
            # parse out file list 
            query=re.compile(adv_re)
            files=[]
            for line in resp_lines:
                match=query.search(line) 
                if match: 
                    files.append(match.group())
            # print("Found %d files in response"%len(files))
            # return [(adv_type,i) for i in sorted(files)]
            rval+=[(adv_type,i) for i in sorted(files)]
        return rval

class NESDISCDNClient(object):
    """
    Http Client for accessing the NESDIS CDN
    """
    URL_BASE                = "https://cdn.star.nesdis.noaa.gov/"
    FLOATER_DATA_BASE       = join(URL_BASE, "FLOATER/data/")
    SECTOR_DATA_BASE        = join(URL_BASE, "%s/SECTOR/%s/%s")
    SECTOR_DATA_CONUS       = join(URL_BASE, "%s/CONUS/%s")
    
    FLOATER_INFO_FILE       = "floaters.txt"
    FLOATER_INFO_URL        = join(FLOATER_DATA_BASE, FLOATER_INFO_FILE)

    @classmethod
    def __get_url(cls, url):
        """
        Get resource at url
        """
        resp=requests.get(url)
        if resp.status_code==404:
            raise ValueError("Resource %s not found on server"%url)
        return resp 

    @classmethod
    def get_image(cls, img_file):
        """
        Get full binary of image from server 
        @return Binary string containing raw jpeg data
        """
        # check if we're getting floater imagery or sector imagery
        source=args.source
        if source.FLOATER:
            base_url=cls.floater_data_url(source.FLOATER_ID, source.BAND)
        else:
            base_url=cls.sector_data_url(source.SECTOR, source.BAND)
        # get the image, be mindfule of nonexistant resources
        img_url=join(base_url,img_file) 
        # return binary image 
        return cls.__get_url(img_url).content

    @classmethod
    def get_image_list(cls, filter_resolution=False):
        """
        Return the list of all 2160p (4K) images in the target dir
        """
        try: IMG_RESOLUTION="%dx%d"%args.image.SOURCE_RES
        except: IMG_RESOLUTION=args.image.SOURCE_RES
        if filter_resolution:
            image_re=IMAGE_RE%IMG_RESOLUTION
        else:
            image_re=IMAGE_RE%""
        # check if we're getting floater imagery or sector imagery
        source=args.source
        if source.FLOATER:
            base_url=cls.floater_data_url(source.FLOATER_ID, source.BAND)
        else:
            base_url=cls.sector_data_url(source.SECTOR, source.BAND)
        # get the image list as html (note: make sure url has trailing backslash)
        base_url=join(base_url,"")
        raw_response=cls.__get_url(base_url).text
        resp_lines=[str(i.strip()) for i in raw_response.split('\n')]
        # print("Got %d response lines"%len(resp_lines))
        # parse out file list 
        query=re.compile(image_re)
        files=[]
        for line in resp_lines:
            match=query.search(line) 
            if match: 
                files.append(match.groups()[0])
        # print("Found %d files in response"%len(files))
        return sorted(files)


    @classmethod 
    def get_floater_ids(cls, storm):
        """
        Query the CDN server for a floater for the named storm 
        """
        # get list of floaters from nesdis cdn txt file
        floaters_md_raw=requests.get(cls.FLOATER_INFO_URL).text
        floaters_csv=[i.strip() for i in floaters_md_raw.split('\n')]
        floaters_csv=[i.split(',') for i in floaters_csv]
        floater_ids=[]
        floaters_set=set() 
        # try to resolve the storm name to a _floater_ id, just in case someone
        # wishes to add a storm by floater id 
        matching_ids=[]
        potential_floater_url=join(cls.FLOATER_DATA_BASE,storm,'') # note: '' is appended to ensure trailing backslash appears
        exists=requests.get(potential_floater_url).status_code==requests.codes.ok
        if exists:
            matching_ids+=[(storm, None)]
        # get names and ids from raw data
        for info in floaters_csv:
            if len(info)!=5: continue
            id_,name = str(info[0]),str(info[-1])
            if name in floaters_set: continue
            floaters_set.add(name)
            floater_ids.append((id_, name))
        # find/return any floaters matching the inputed named storm
        matching_ids+=[(id_,name) for (id_,name) in floater_ids if storm.lower() in name.lower() ]
        return matching_ids


    @classmethod 
    def sector_data_url(cls, sector, band):
        """
        Get the CDN http URL for the given imagery
        """
        # check that the sector exists, and get the related imager
        imager=None
        for scid, spacecraft in imagers.all():
            try: sector=spacecraft.sectors.get(sector)
            except: continue  
            imager=spacecraft.IMAGER 
            break 
        else:
            msg="Sector %s not recognized"
            raise ValueError(msg%sector)
        # check that the band is recognized 
        try: band=bands.get(band)
        except: raise ValueError("Band %s not recognized"%band)
        # create url
        if sector==imagers.GOES16.sectors.CONUS:
            return cls.SECTOR_DATA_CONUS%(imager,band)
        else:
            return cls.SECTOR_DATA_BASE%(imager, sector, band)

    @classmethod 
    def floater_data_url(cls, floater_id, band):
        """
        Get the CDN http URL for the given floater imagery
        """
        # check that the band is recognized 
        try: band=bands.get(band)
        except: raise ValueError("Band %s not recognized"%band)
        floater_data_path=join(floater_id,band)
        # create url
        return join(cls.FLOATER_DATA_BASE, floater_data_path)