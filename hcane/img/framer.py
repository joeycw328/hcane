from hcane.runtime import args 
from hcane.basic.framer import FramerIf

from .view import ViewPortFOV, ViewPort

class BasicFramer(FramerIf):
    """
    Default framer implementation. Provides static midpoint 
    cropping based on configuration parameters via standard 
    ViewPort
    """
    def __init__(self, queue_dir, frame_dir):
        """
        Create viewport and basic fov
        """
        # superclass init 
        super(BasicFramer, self).__init__(queue_dir,frame_dir)
        # create viewport
        raw_width,raw_height=args.image.SOURCE_RES
        tgt_width,tgt_height=args.image.TARGET_RES
        view_height=min(tgt_height,raw_height)
        view_width=int(view_height*(16./9.))
        fov=ViewPortFOV(raw_width,raw_height,view_width, view_height)
        self.viewport=ViewPort(fov)

    @property  
    def final_width(self): return self.viewport.fov.view.width 

    @property  
    def final_height(self): return self.viewport.fov.view.height 
    
    def prepare_list(self, image_list):
        """
        Default implementation returns image_list as is
        """
        return [(image,) for image in image_list]

    def dispatch(self, pil_image, *params):
        """
        Set center and return cropped image
        """
        x=args.image.HOFFSET
        y=args.image.OFFSET 
        x,y=self.viewport.fov.to_pil(x,y)
        self.viewport.set_center(x,y)
        return self.viewport.create_view(pil_image)
