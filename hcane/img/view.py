import threading 
from hcane.map.transform import BiasedFOV #, BiasedGeodesicTransformation

class ViewPortView(object):
    def __init__(self, fov, viewport_width, viewport_height):
        self.width=viewport_width 
        self.height=viewport_height 
        self.max_x_bias=fov.x_max-self.width
        self.max_y_bias=fov.y_max-self.height
    
    center=property(lambda self: (self.width/2, self.height/2))

class ViewPortFOV(BiasedFOV):
    """
    Subclass to manage parameters specific to goes imagery resolutions
    """
    def __init__(self, img_width, img_height, view_width=3840, view_height=2160):
        # init superclass 
        super(ViewPortFOV, self).__init__(img_width, img_height)
        # create view metadata container; update origin to image center 
        self.view=ViewPortView(self, view_width, view_height)
        self.set(origin=self.center)
        

class ViewPort(object):
    """
    Facility for cropping pil images based on midpoint
    """
    def __init__(self,fov):
        if not isinstance(fov, ViewPortFOV):
            msg="Parameter fov expected type %s got %s"
            msg=msg%(ViewPortFOV.__name__,type(fov).__name__)
            raise TypeError(msg)
        self.fov=fov 
        self.__local=threading.local()
        self.__local.__x_offset=0
        self.__local.__y_offset=0
    
    offset=property(lambda self: (self.__local.__x_offset, self.__local.__y_offset))

    @property 
    def center(self):
        """
        Return midpoint of viewport in pil coordinates
        """
        view_center_x,view_center_y=self.fov.view.center
        center_x=self.__local.__x_offset+view_center_x 
        center_y=self.__local.__y_offset+view_center_y 
        return center_x, center_y

    def set_offset(self, x, y):
        """
        Update offset (in PIL coordinates), minding the bounds of the fov
        """
        self.__local.__x_offset=max(0, min(x, self.fov.view.max_x_bias))
        self.__local.__y_offset=max(0, min(y, self.fov.view.max_y_bias))

    def set_center(self, x, y):
        """
        Update center (in PIL coordinates), minding the bounds of the fov
        """
        center_x,center_y=self.fov.view.center
        self.set_offset(x-center_x, y-center_y)

    def create_view(self, pil_image):
        """
        Return pil_image cropped to the parameters specified by the fov
        """
        left=self.__local.__x_offset
        top=self.__local.__y_offset
        right=self.__local.__x_offset+self.fov.view.width
        bottom=self.__local.__y_offset+self.fov.view.height
        return pil_image.crop((left,top,right,bottom))
