import os
import datetime
from os.path import join, dirname, basename
from operator import add, sub
from PIL import Image, ImageFont, ImageDraw 
from threading import Lock
from hcane.runtime import args 

IMG_DIRECTORY=dirname(__file__)
CREDIT_BAR_FILENAME="credit_bar.jpg"
HELVETICA_FILENAME="helvetica.ttf"
# date, time (zulu), s/c (goes16/17), band
CREDIT_TXT="%s %sZ CIRA/NOAA %s %s"
with Image.open(join(IMG_DIRECTORY, CREDIT_BAR_FILENAME)) as img_:
    CREDIT_BAR=img_.copy()
HELVETICA_PATH=join(IMG_DIRECTORY, HELVETICA_FILENAME)

credit_bar_access=Lock()

def adjust(crop_box, vpixels, hpixels=0):
    "Move the input up or down by x pixels in PIL coordinates"
    adjustment=(-1*hpixels,vpixels,-1*hpixels,vpixels)
    return tuple(map(sub, crop_box, adjustment))


def make_credit(pil_image,src_filename):
    """
    Use the information in the filename to create an informative credit bar

    Extract the date, time, satellite name, and band from the image filename 
    and use it to populate text on a white credit bar at the bottom of the 
    processed frame. Always good to give credit where credit is due
    """
    # get the original image filename and tokenize it 
    raw_img_filename=src_filename 
    try: raw_img_filename=basename(os.readlink(raw_img_filename))
    except: pass
    img_base=raw_img_filename.split('.')[0]
    integer_date,img_metadata=img_base.split('_')
    if args.source.FLOATER:
        spacecraft, ign, ign, band, sector, ign=img_metadata.split('-')
    else:    
        spacecraft, ign, sector, band, ign=img_metadata.split('-')
    # build the credit string
    year=integer_date[:4]; days=integer_date[4:7]; time=integer_date[7:]
    full_date=datetime.datetime(int(year),1,1)+datetime.timedelta(int(days)-1)
    date=full_date.strftime("%d %B %Y")
    time="%s:%s"%(time[:2],time[2:])
    credit_str=CREDIT_TXT%(date, time, spacecraft, band)
    # get a copy of the credit bar so we don't fuck up the original 
    with credit_bar_access:
        credit_bar=CREDIT_BAR.copy()
    # draw the credit text on the credit bar
    drawer=ImageDraw.Draw(credit_bar)
    font=ImageFont.truetype(HELVETICA_PATH, size=40)
    hoffset=font.getsize(credit_str)[0]/2
    drawer.text((2000-hoffset,4), credit_str, font=font, fill="black")
    # crop and return the credit bar
    buffer=(credit_bar.width-pil_image.width)/2
    if buffer>0:
        return credit_bar.crop((buffer,0,buffer+pil_image.width, credit_bar.height))
    else:
        return credit_bar


def add_credit(pil_image, credit=None):
    """
    Use the information in the filename to create an informative credit bar

    Extract the date, time, satellite name, and band from the image filename 
    and use it to populate text on a white credit bar at the bottom of the 
    processed frame. Always good to give credit where credit is due
    """
    credit_=credit if credit!=None else make_credit(pil_image)
    width=pil_image.width
    height=pil_image.height
    pil_image.paste(credit_, (0,height-40,width,height))
    
