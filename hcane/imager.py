"""
NESDIS server configuration information 

Provides useful information relating GOES sectors to their respective FTP server
endpoints to allow scraping images from sever GOES sectors

@file hcane/server.py
@author Josef C. Willhite
@date 13 July 2019
"""

from .basic.key import KeyManager, Key

class imagers(KeyManager):
    """
    Keys for mapping GOES imagers to their URL data paths
    """

    class GOES16(KeyManager):
        """
        Keys related to the GOES16 Advanced Baseline Imager (ABI)
        """
        IMAGER              = Key("GOES16/ABI")
        
        class sectors(KeyManager):
            """
            GOES16 ABI sector keys
            """
            GULF_OF_MEXICO          = Key("gm")
            US_EAST_COAST           = Key("eus")
            CONUS                   = Key("CONUS")
            CARIBBEAN               = Key("car")
            TROPICAL_ATLANTIC       = Key("taw")
            EASTERN_EAST_PACIFIC    = Key("eep")
            CENTRAL_AMERICA         = Key("cam")
            MEXICO                  = Key("mex")
    
    class GOES17(KeyManager):
        """
        Keys related to the GOES17 Advanced Baseline Imager (ABI)
        """
        IMAGER              = Key("GOES17/ABI")
        
        class sectors(KeyManager):
            """
            GOES16 ABI sector keys
            """
            TROPICAL_PACIFIC    = Key("tpw")
            US_PACIFIC_COAST    = Key("wus")


class bands(KeyManager):
    """
    Keys for mapping GOES imager bands to their URL data paths
    """
    GEOCOLOR            = Key("GEOCOLOR")