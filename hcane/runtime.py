"""
Runtime configuration settings

Runtime configuration information including on-disk directory locations, storm 
name and configuration settings, etc.

@file hcane/runtime.py
@author Josef C. Willhite
@date 15 July 2019
"""
import logging
import os
import time
from os.path import join, exists
from collections import OrderedDict
from configparser import ConfigParser 

HCANED_CONFIG_DIR   = "/etc/hcaned"
HCANED_CONFIG_FILE  = join(HCANED_CONFIG_DIR, "hcaned.conf")

STORMFILE_SUFFIX    = ".ini"

# every image on the NESDIS server begins with string 'YYYYDDD' where 
# 'YYYY' is the year, and 'DDD' is the integral day of the year (e.g. '203' for
# July 22nd ). We use this (converted to an integer) to calculate start dates
INTEGER_YEAR        = time.localtime().tm_year
INTEGER_DAY         = time.localtime().tm_yday 
DEFAULT_START       = int(str(INTEGER_YEAR)+str(INTEGER_DAY))

log=logging.getLogger("hcaned.runtime")


class UConfigParser(ConfigParser):
    """A config parser that only deals in upper case things"""
    def __init__(self, *args, **kwargs):
        ConfigParser.__init__(self, *args, **kwargs)
    def optionxform(self, string): 
        return ConfigParser.optionxform(self, string).upper()

class args:

    class daemon:
        ROOTDIR     = "/var/lib/hcaned"
        DATADIR     = "data"
        STORMDIR    = "storms"
        ARCHIVEDIR  = "archive"
        LOGLEVEL    = "INFO"


    class metadata:
        NAME        = None 
        ID          = '' 
    
    class image: 
        
        class framers:
            LEGACY      = "LEGACY"
            EYELEVEL    = "EYELEVEL"
            NONE        = ''
       
        FPS         = "30" 
        OFFSET      = "0"
        HOFFSET     = "0"
        SOURCE_RES  = "4000x4000"
        TARGET_VRES = "2160"
        # Note: This is created at runtime!!
        # TARGET_RES = (3840,2160)
        ADD_CREDIT  = "True"
        REQUEUE     = False
        Q_ONLY      = False
        FRAMER      = framers.LEGACY

    class source:
        SECTOR      = "GULF_OF_MEXICO"
        BAND        = "GEOCOLOR"
        STARTDATE   = DEFAULT_START
        FLOATER     = False 
        FLOATER_ID  = ""

daemon_defaults={
    "daemon": {
        "ROOTDIR":      args.daemon.ROOTDIR,
        "DATADIR":      args.daemon.DATADIR,
        "STORMDIR":     args.daemon.STORMDIR,
        "ARCHIVEDIR":   args.daemon.ARCHIVEDIR,
        "LOGLEVEL":     args.daemon.LOGLEVEL
    }
}

storm_defaults={
    "metadata": {
        "ID":           args.metadata.ID
    },
    "image": {
        "FPS":          args.image.FPS ,
        "OFFSET":       args.image.OFFSET,
        "HOFFSET":      args.image.HOFFSET,
        "SOURCE_RES":   args.image.SOURCE_RES,
        "TARGET_VRES":  args.image.TARGET_VRES,
        "ADD_CREDIT":   args.image.ADD_CREDIT,
        "FRAMER":       args.image.FRAMER
    },
    "source": {
        "SECTOR":       args.source.SECTOR,
        "BAND":         args.source.BAND,
        "STARTDATE":    args.source.STARTDATE,
        "FLOATER":      args.source.FLOATER,
        "FLOATER_ID":   args.source.FLOATER_ID,
    },
    
}

def init_daemon():
    # make sure the root config exists
    if not exists(HCANED_CONFIG_FILE):
        msg="Config file:\n  %s\nNot found!"%HCANED_CONFIG_FILE
        print(msg )
        return False
    # parse the root config
    defaults={}
    for sec, options in daemon_defaults.items():
        defaults.update(options)
    parser=UConfigParser(defaults)
    parser.read(HCANED_CONFIG_FILE)
    # update the global configs
    for sec_name,options in daemon_defaults.items():
        for option, default_value in options.items():
            final_value=parser.get(sec_name, option)
            glargs_class=getattr(args, sec_name)
            setattr(glargs_class, option, final_value)
    # postprocess the root args 
    daemon=args.daemon
    daemon.DATADIR = join(daemon.ROOTDIR, daemon.DATADIR)
    daemon.STORMDIR = join(daemon.ROOTDIR, daemon.STORMDIR)
    daemon.ARCHIVEDIR = join(daemon.ROOTDIR, daemon.ARCHIVEDIR)
    daemon.LOGLEVEL = getattr(logging, daemon.LOGLEVEL)
    try:
        if not exists(daemon.DATADIR): 
            log.warn("Data directory doesn't exist!")
            log.info("Creating data directory: %s"%daemon.DATADIR)
            # ensure the process has sufficient privileges
            if not has_required_privileges(): return False
            os.makedirs(daemon.DATADIR)
        if not exists(daemon.STORMDIR): 
            log.warn("Storm directory doesn't exist!")
            log.info("Creating storm directory: %s"%daemon.STORMDIR)
            # ensure the process has sufficient privileges
            if not has_required_privileges(): return False
            os.makedirs(daemon.STORMDIR)
        if not exists(daemon.ARCHIVEDIR): 
            log.warn("Archive directory doesn't exist!")
            log.info("Creating archive directory: %s"%daemon.ARCHIVEDIR)
            # ensure the process has sufficient privileges
            if not has_required_privileges(): return False
            os.makedirs(daemon.ARCHIVEDIR)
    except OSError as e:
        msg="Failed to create directory: %s"%str(e)
        log.error(msg)
        return False
    return True

def cl_override(clargs):
    # hcaned specific configuration args
    try:
        args.image.REQUEUE=clargs.requeue
        args.image.Q_ONLY=clargs.queue_only
    except:
        pass
    # framer specific args
    try:    
        args.image.TARGET_VRES=clargs.TARGET_VRES
        args.image.ADD_CREDIT=clargs.ADD_CREDIT
    except: pass
    

def init(clargs):
    """
    Fill out global args from clargs
    """
    if not init_daemon(): return False
    # update the storm name 
    args.metadata.NAME=clargs.name
    stormfile_name=args.metadata.NAME+STORMFILE_SUFFIX
    storm_cfgfile=join(args.daemon.STORMDIR, stormfile_name)
    # make sure the storm config exists
    if not exists(storm_cfgfile):
        msg="Storm config:\n  %s\nNot found!"%storm_cfgfile
        print(msg )
        msg="Please run:\n  hcaned -c %s\nTo configure this storm"
        print(msg%args.metadata.NAME)
        return False
    # parse the storm config file
    defaults={}
    for sec, options in storm_defaults.items():
        defaults.update(options)
    parser=UConfigParser(defaults)  
    parser.read(storm_cfgfile)
    # update the global configs
    for sec_name,options in storm_defaults.items():
        for option, default_value in options.items():
            final_value=parser.get(sec_name, option)
            glargs_class=getattr(args, sec_name)
            setattr(glargs_class, option, final_value)
    # apply command line overrides
    cl_override(clargs)
    # postprocess storm config
    image=args.image 
    image.FPS           = int(image.FPS)
    image.OFFSET        = int(image.OFFSET)
    image.HOFFSET       = int(image.HOFFSET)
    image.ADD_CREDIT    = str(image.ADD_CREDIT).lower()=="true"
    image.SOURCE_RES    = tuple([int(i) for i in image.SOURCE_RES.split('x')])
    image.TARGET_VRES   = int(image.TARGET_VRES)
    image.TARGET_RES    = tuple([int((16./9.)*image.TARGET_VRES),image.TARGET_VRES])
    source=args.source 
    source.FLOATER      = str(source.FLOATER).lower()=="true"
    if source.FLOATER and not source.FLOATER_ID:
        print("source.FLOATER_ID cannot be empty if source.FLOATER set to 'True'")
        return False
    
    return True
        
def has_required_privileges():
    """
    Ensures user has permissions to access the hcaned data directory
    """
    try:
        # touch the .su file in the root config directory, creating it if it 
        # doesn't exists. This suffices to ensure the current user has sufficient 
        # priveleges
        sufile=join(HCANED_CONFIG_DIR, ".su")
        if not exists(sufile): 
            with open(sufile, "w+") as touched: pass 
        else:
            os.utime(sufile, None)
        return True
    # permission denied while trying to create the .su file
    except IOError as e:
        log.debug("Privilege check failed: %s"%str(e))
        print("You have insufficient permissions to perform that action!")
        return False
    # permission denied while trying to touch the .su file
    except OSError as e:
        log.debug("Privilege check failed: %s"%str(e))
        print("You have insufficient permissions to perform that action!")
        return False
    # some other error
    except:
        raise
    
    

ENV_DEBUG_VAR       = "HCANE_DEBUG"
ENV_DBGPRT_VAR      = "HCANE_PORT"
DBGPORT_DEFAULT     = 8888

def enable_debug(port=DBGPORT_DEFAULT):
    # import ptvsd and start debugging on the inputed port and default host
    import ptvsd 
    import socket
    # ip=socket.gethostbyname(socket.gethostname())
    ip="0.0.0.0"
    log.info("Starting ptvsd debugger")
    ptvsd.enable_attach(address=(ip, port), redirect_output=False)
    log.info("Listening on host/port: %s:%s"%(ip,port))
    log.info("Waiting for client attach...")
    ptvsd.wait_for_attach()
    log.info("Good client attach detected. Breaking into debugger.")
    ptvsd.break_into_debugger()

# # start with ptvsd debugging enabled if prompted via environment variable
# if ENV_DBGPRT_VAR in os.environ: 
#     dbg_port=os.environ[ENV_DBGPRT_VAR]
# else: 
#     dbg_port=DBGPORT_DEFAULT
# if ENV_DEBUG_VAR in os.environ: 
#     enable_debug(dbg_port)
    
params=None