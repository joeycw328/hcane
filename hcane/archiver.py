import os
import tarfile
from os.path import join, basename, exists
import logging 
import shutil
import traceback 
import time

from . import hcane

log=None

def run(args):
    """
    Run the framer routine
    """
    global OFFSET 
    global log 
    global QUEUE_DIR
    global FRAME_DIR
    global pool
    global ADD_CREDIT
    os.umask(0o022)
    # get logger
    log=logging.getLogger("hcane.%s.archiver"%args.metadata.NAME)
    log.info("Booting archiver for storm: %s"%args.metadata.NAME)
    # get data directories from loaded configuration
    storm_data_dir=join(args.daemon.DATADIR, args.metadata.NAME)
    FRAME_DIR=join(storm_data_dir, hcane.FRAME_SUBDIR)
    log.info("Looking for frames in directory: %s"%FRAME_DIR)
    all_frames=sorted(os.listdir(FRAME_DIR))
    if not all_frames:
        log.info("No frames discovered!")
        return True
    # create the name of the archive, and aggregate directory
    start_frame=all_frames[0].split('_')[0]
    end_frame=all_frames[-1].split('_')[0]
    log.info("Discovered %d frames spanning %s to %s"%(len(all_frames), start_frame, end_frame))
    arch_name="%s_%s-%s_framed"%(args.metadata.NAME, start_frame, end_frame)
    all_name="%s_all_framed"%args.metadata.NAME
    # make archive 
    arch_fullpath=join(args.daemon.ARCHIVEDIR, arch_name)
    arch_fullpath='.'.join([arch_fullpath,"tar"])
    arch_tmppath=join(FRAME_DIR, arch_name)
    arch_allpath=join(FRAME_DIR, all_name)
    start=time.time()
    try:
        # move all images into a usefully named directory 
        if not exists(arch_tmppath): 
            log.info("Creating archive tmp directory: %s"%arch_tmppath)
            os.makedirs(arch_tmppath)
        if not exists(arch_allpath): 
            log.info("Creating archive aggregate directory: %s"%arch_allpath)
            os.makedirs(arch_allpath)
        log.info("Moving images to archive directories...")
        os.chdir(arch_allpath)
        for image in all_frames: 
            og_path=join(FRAME_DIR, image)
            log.debug("%s --> %s"%(og_path, arch_tmppath))
            shutil.move(og_path, arch_tmppath)
            os.link(join("..",arch_name,image),join(arch_allpath,image))
        log.info("Creating archive ...")
        archive=tarfile.open(arch_fullpath, "w")
        os.chdir(FRAME_DIR)
        archive.add(arch_name)
        archive.add(all_name)
        archive.close()
        # tarfile=shutil.make_archive(arch_fullpath, "tar", FRAME_DIR, arch_name) 
        log.info("Archive success!")
    except:
        for line in traceback.format_exc().strip().split('\n'):
            log.critical(line)
        log.error("Archiving failed.")
        return False
    duration=time.time()-start
    log.info("Cleaning up...")
    shutil.rmtree(arch_tmppath)
    log.debug("Removed archive tmp directory: %s"%arch_tmppath)
    shutil.rmtree(arch_allpath)
    log.debug("Removed archive aggregate directory: %s"%arch_allpath)
    log.info("Cleanup success!")
    log.info("Archived %d frames in %.2f seconds"%(len(all_frames), duration))
    log.info("Wrote archive: %s"%arch_fullpath)
    return True
    


    