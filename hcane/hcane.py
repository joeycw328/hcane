#!/usr/bin/env python
"""
Imagery aggregation engine

Multiprocessing engine for scraping imagery from NESDIS server. Wraps the FTP client
implementation in hcane.client to provide parallelization, disk IO, and frame 
processing preparation

@file hcane/hcane.py
@author Josef C. Willhite
@date 15 July 2019
"""
import os
import time
from os import symlink
from os.path import join, exists
from ftplib import FTP
import logging
from contextlib import contextmanager
from multiprocessing.pool import Pool, ThreadPool 
import signal 
import traceback as tb
import errno

import threading

# from . import server
from .imager import imagers
# from .client import NESDISClient as NESDIS
from .client import NESDISCDNClient as Client
from .runtime import args 

HCANE_NAME	        = None
# TARGET_FTP_DIR	    = "pub/star/goes/GOES16/ABI/SECTOR/gm/GEOCOLOR/"
TARGET_FTP_DIR	    = None
QUEUE_SUBDIR        = "queue"
LINK_TEXT           = "hcane-%s-%04d.jpg"
FRAME_SUBDIR        = "frames"
LOG_LEVEL	        = logging.INFO
IMG_RESOLUTION      = None
TOTAL_IMAGES        = 0
FORCE_REQUEUE       = False
NUM_PROCS           = 2
THREADS_PER_PROC    = 4
# setup logging
log=None 

# setup parallelization 
def worker_setup():
    # this_thread.client=NESDIS(TARGET_FTP_DIR)
    this_thread.client=Client
    log.info("Created connection to server: %s"%str(this_thread.client))

pool=None
this_thread=threading.local()
CONTINUE=True
received=0
total=0
def job(img_idx, destdir):
    if not CONTINUE: return
    global received
    global total
    # get the name and path on-disk where to place the image
    img_name=hi_def[img_idx]
    img_path='/'.join([destdir, img_name])
    raw_jpeg=""
    # no point double-downloading images that already exist
    if exists(img_path):
        log.info("[%04d/%04d] Skipping Image %s: already exists! "%(img_idx,total,img_name))
        if not FORCE_REQUEUE: 
            log.debug("Skipping requeue: %s"%str(not FORCE_REQUEUE))
            return 
    # here we scrape the imagery
    else:
        try:
            # try to get the image using this thread's client
            log.info("[%04d/%04d] Scraping image: %s"%(img_idx,total,img_name))
            raw_jpeg=this_thread.client.get_image(img_name)
        except EOFError:
            # if the connection is dropped, reconnect and retry
            log.warn("[%04d/%04d] Server closed connection. Retrying.."%(img_idx,total))
            this_thread.client.connect()
            raw_jpeg=this_thread.client.get_image(img_name)
        # data come as binary jpegs. dump them to disk
        with open(img_path, "wb+") as jpeg:
            log.info("[%04d/%04d] Dumping image cache: %s"%(img_idx,total,img_name))
            jpeg.write(raw_jpeg)
        received+=1
    # symlink the newly received image into the frame processing queue so the 
    # framed processing daemon can deal with them
    log.debug("proceeding to requeue...")
    # link_name=LINK_TEXT%(HCANE_NAME, img_idx)
    link_name=img_name
    link_dir=join(destdir,QUEUE_SUBDIR)
    link_path=join(link_dir, link_name)
    if exists(link_path): 
        log.debug("Link %s already exists"%link_path)
        link_end=os.readlink(link_path)
        link_end=join(link_dir, link_end)
        if not exists(link_end):
            log.warn("Link %s is stale. Removing."%link_path)
            os.remove(link_path)
        else:
            log.debug("Skipping exising link.")
            return
    img_relpath=join("..",img_name)
    log.debug("[%04d/%04d] Linking %s --> %s"%(img_idx,total,img_path, link_path))
    os.symlink(img_relpath, link_path)


class JobFailureError(Exception):
    """Error raised for failed jobs"""


def scrape_imagery(raw_data_dir="/var/lib/hcane", hi_def=None, start_idx=0):
    global CONTINUE
    global received
    global log
    global total
    log=logging.getLogger("hcane.%s"%HCANE_NAME)
    log.setLevel(LOG_LEVEL)

    # get the directory in which to store raw data
    HCANE_DATA_DIR=join(raw_data_dir, HCANE_NAME)
    
    # start the worker pool
    pool=ThreadPool(THREADS_PER_PROC, worker_setup)

    # scrape data 
    log.info("Discovered %d images..."%len(hi_def))
    cur=start_idx
    total=len(hi_def)
    received=0
    LINK_TO_LATEST='.'.join([IMG_RESOLUTION,"jpg"])
    log.info("latest image: %s"%LINK_TO_LATEST)
    all_results=[]
    for i in range(int(total/2)-1):
        cur=start_idx+i
        # ignore the link to the latest image 
        if hi_def[cur]==LINK_TO_LATEST:
            log.warn("skipping link to latest: %s"%hi_def[cur])
            continue
        def try_job(cur):
            try: job(cur, HCANE_DATA_DIR)
            except: return tb.format_exc()
        
        all_results.append(pool.apply_async(try_job,(cur,)))

    pool.close()
    exception=None
    # process results
    try:
        for job_result in all_results:
            res=job_result.get()
            if res: 
                CONTINUE=False; exception=res
                raise JobFailureError()
    except KeyboardInterrupt:
        log.warn("Operation aborted!")
        log.info("Terminating worker threads")
        pool.terminate()
    except JobFailureError:
        log.error("A job has failed!!")
        log.info("Terminating worker threads")
        pool.terminate()
    except:
        log.error("Unknown error")
        log.info("Terminating worker threads")
        pool.terminate()
        exception=traceback.format_exc()
    log.warn("Waiting for threads shutdown...")
    pool.join()
    return exception

def parallel_job(*args):
    try: return scrape_imagery(*args)
    except: return tb.format_exc()

def get_image_list():
    # get the image list from nesdis
    log.info("Getting image list from nesdis server: %s"%Client.URL_BASE)
    hi_def=Client.get_image_list(filter_resolution=True)
    log.info("Success.")
    min_start_date=int(args.source.STARTDATE)
    log.info("Pruning imagery from before: %d"%min_start_date)
    time.sleep(3)
    # prune old imagery by start date
    images_to_keep=[]
    for img_filename in hi_def:
        try: img_date=int(img_filename[:7])
        except: continue 
        if img_date < min_start_date: 
            log.info("Skipping old image: %s"%img_filename)
            continue 
        images_to_keep.append(img_filename)
    return sorted(images_to_keep)

def x_scrape_imagery_parallel(raw_data_dir="/var/lib/hcane"):
    global pool
    global hi_def
    global received
    global log
    global total
    # global TARGET_FTP_DIR
    global IMG_RESOLUTION
    global FORCE_REQUEUE
    log=logging.getLogger("hcane.%s"%HCANE_NAME)
    log.setLevel(LOG_LEVEL)
    # TARGET_FTP_DIR=server.sectors.get(args.source.SECTOR)
    log.debug(args.image.SOURCE_RES)
    IMG_RESOLUTION="%dx%d"%args.image.SOURCE_RES
    FORCE_REQUEUE=args.image.REQUEUE or args.image.Q_ONLY
    # create the directory in which to store raw data
    HCANE_DATA_DIR=join(raw_data_dir, HCANE_NAME)
    if not exists(HCANE_DATA_DIR):
        log.warn("Storm directory doesn't exist: %s"%HCANE_DATA_DIR)
        log.info("Creating storm directory: %s"%HCANE_DATA_DIR)
        # prevent a race condition
        try: os.makedirs(HCANE_DATA_DIR)
        except OSError as e:
            if e.errno!=errno.EEXIST: raise 
            log.warn("Ignoring race condition")
    queue_dir=join(HCANE_DATA_DIR,QUEUE_SUBDIR)
    if not exists(queue_dir):
        log.warn("Queue directory doesn't exist: %s"%queue_dir)
        log.info("Creating queue directory: %s"%queue_dir)
        os.makedirs(queue_dir)
    frame_dir=join(HCANE_DATA_DIR,FRAME_SUBDIR)
    if not exists(frame_dir):
        log.warn("Frame directory doesn't exist: %s"%frame_dir)
        log.info("Creating frame directory: %s"%frame_dir)
        os.makedirs(frame_dir)
    # get images from nesdis 
    if args.image.Q_ONLY:
        log.warn("Skipping download. Refreshing framer queue.")
        hi_def=sorted([i for i in os.listdir(HCANE_DATA_DIR) if ".jpg" in i])
    else:
        hi_def=get_image_list()
    # create a few process to do work
    all_results=[]
    pool=Pool(NUM_PROCS)
    mid_idx=int(len(hi_def)/2)-1
    proc_args=(raw_data_dir, hi_def, 0)
    all_results.append(pool.apply_async(parallel_job,proc_args))
    proc_args=(raw_data_dir, hi_def, mid_idx+1)
    all_results.append(pool.apply_async(parallel_job,proc_args))

    pool.close()
    return all_results