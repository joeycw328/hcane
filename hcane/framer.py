"""
Frame processing engine

Contains implementations of the PIL image processing package to post-process 
raw frames from the NESDIS server. Chops frames down to 16:9 aspect ratio and 
moves the credit and timestamp information into view

@file hcane/framer.py
@author Josef C. Willhite
@date 15 July 2019
"""
import os 
from os.path import join, basename, exists, dirname, realpath
import time
import logging 
from PIL import Image
from multiprocessing.pool import ThreadPool

from . import hcane
from .img.framer import BasicFramer
from .img.credit import add_credit, make_credit

from hcane.eyelevel.framer import EyeLevelFramer

# These are initialized by the run function. They are set from global args in
# the runtime.args module, and so are initialized after the module is. This may 
# be unnecessary, but it may be necessary to support multiprocessing 
POOL_SIZE   = 4
ADD_CREDIT  = True
log         = None
pool        = None
FRAMER      = None

_total      = 0
_failed     = 0

def job(queued_filename, *args):
    """
    Transform the image using the transform module, and output to the frames 
    directory
    """
    global _total
    global _failed
    try:
        # get full path to frame
        path_to_queued_frame=FRAMER.enqueued_path(queued_filename)
        infile_str=join(basename(FRAMER.queue_dir), queued_filename)
        # use the true name of the image (not the symlink) to derive the final 
        # filename.
        processed_frame_stub=basename(FRAMER.resolved_path(queued_filename))
        processed_frame_stub='-'.join(processed_frame_stub.split('-')[:-1])
        size_str='%dx%d'%(FRAMER.final_width,FRAMER.final_height)
        processed_frame_name='-'.join([processed_frame_stub,size_str])
        processed_frame_name+=".jpg"
        path_to_final_frame=join(FRAMER.frame_dir, processed_frame_name)
        outfile_str=join(basename(FRAMER.frame_dir), processed_frame_name)
        # skip frames that already exist
        if exists(path_to_final_frame):
            log.warning("Frame '%s' already exists! Skipping."%processed_frame_name)
        # otherwise, do the image processing and dump the final image
        else:
            raw_image=Image.open(path_to_queued_frame)
            final_image=FRAMER.dispatch(raw_image, *args)
            if ADD_CREDIT:
                add_credit(final_image, make_credit(final_image,raw_image.filename))
            # add_credit(final_image)
            msg="Framed: %s --> %s"
            log.info(msg%(infile_str, outfile_str))
            final_image.save(
                path_to_final_frame, dpi=(72,72),
                icc_profile=raw_image.info.get('icc_profile')
            )
            _total+=1
        # clean the queued frame either way
        log.info("Cleaning up queued item: %s"%infile_str)
        os.remove(path_to_queued_frame)
    except IOError as e:
        import traceback as tb 
        for line in tb.format_exc().strip().split('\n'):
            log.critical(line)
        log.warn("Job '%s' failed: %s"%(queued_filename, str(e)))
        linkend=os.readlink(path_to_queued_frame)
        startdir=dirname(path_to_queued_frame)
        path_to_src_image=realpath(join(startdir,linkend))
        log.warn("Rescheduling image for download: %s"%path_to_src_image)
        os.remove(path_to_queued_frame)
        os.remove(path_to_src_image)
        _failed+=1
    except Exception as e:
        log.error("Job '%s' failed: %s"%(queued_filename, str(e)))
        import traceback as tb 
        _failed+=1
        return tb.format_exc().strip()


def post_job(job_func, image_file, *args):
    """
    Schedule the next job for execution
    """
    # for now this just runs the job and returns the result 
    log.info("Dispatching job for image: %s"%image_file)
    return pool.apply_async(job_func, (image_file, args))


def shutdown():
    """
    Shutdown the job processor
    """
    # does nothing for now 
    log.info("Closing pool.")
    pool.close()
    pool.terminate()
    log.info("Waiting for jobs to finish...")
    pool.join()


def run(args):
    """
    Run the framer routine
    """
    global log 
    global pool
    global ADD_CREDIT
    global FRAMER
    global _total
    os.umask(0o022)
    # get logger
    log=logging.getLogger("hcane.%s.framer"%args.metadata.NAME)
    log.info("Booting framer for storm: %s..."%args.metadata.NAME)
    # allow explicitly disabling the framer daemon via configuration
    if args.image.FRAMER==args.image.framers.NONE:
        log.warning("Framer has been explicitly disabled. Skipping.")
        return 0
    ADD_CREDIT=args.image.ADD_CREDIT
    log.info("Adding Image Credit: %s"%bool(ADD_CREDIT))
    # get data directories from loaded configuration
    storm_data_dir=join(args.daemon.DATADIR, args.metadata.NAME)
    QUEUE_DIR=join(storm_data_dir, hcane.QUEUE_SUBDIR)
    FRAME_DIR=join(storm_data_dir, hcane.FRAME_SUBDIR)
    log.info("Placing framed images in directory: %s"%FRAME_DIR)
    # create framer
    if args.image.FRAMER==args.image.framers.EYELEVEL:
        log.info("Using EyeLevel framer...")
        FRAMER=EyeLevelFramer(QUEUE_DIR, FRAME_DIR)
    elif args.image.FRAMER==args.image.framers.LEGACY:
        log.info("Using legacy framer...")
        log.info("Image vertical offset: %+d"%args.image.OFFSET)
        log.info("Image horizontal offset: %+d"%args.image.HOFFSET)
        FRAMER=BasicFramer(QUEUE_DIR, FRAME_DIR)
    else:
        log.error("Unrecognized framer configuration: %s"%args.image.FRAMER)
        return 1
    # create the directory in which to store raw data
    log.info("Looking for raw images in directory: %s"%QUEUE_DIR)
    if not exists(QUEUE_DIR):
        log.warn("Queue directory doesn't exist: %s"%QUEUE_DIR)
        log.info("Creating queue directory: %s"%QUEUE_DIR)
        os.makedirs(QUEUE_DIR)
    if not exists(FRAME_DIR):
        log.warn("Frame directory doesn't exist: %s"%FRAME_DIR)
        log.info("Creating frame directory: %s"%FRAME_DIR)
        os.makedirs(FRAME_DIR)
    # get the list of queued raw data frames 
    log.info("Running discovery...")
    start=time.time()
    raw_frame_queue=sorted(os.listdir(QUEUE_DIR))
    frame_count=len(raw_frame_queue)
    if not frame_count:
        log.warn("No new frames discovered.")
        log.info("Exiting.")
        return False
    log.info("Discovered %d new frames!"%frame_count)
    # let the framer preprocess the frame list, exit if it finds nothing suitable
    log.info("Running preprocessor...")
    framer_params_list=FRAMER.prepare_list(raw_frame_queue)
    log.info("Frame preprocessor returned %d results."%len(framer_params_list))
    if not framer_params_list:
        log.info("Exiting.")
        return False
    # dispatch them to be processed
    log.info("Dispatching...")
    pool=ThreadPool(POOL_SIZE)
    results=[]
    for params in framer_params_list:
        results.append(post_job(job, *params))
    # validate results
    failure=False
    try:
        for result in results:
            # a successful job returns nothing
            result=result.get()
            if not result: 
                continue
            # failed jobs return raw tracebacks, so we log them as critical 
            for line in result.split('\n'):
                log.critical(line)
            # a single job failure is sufficient for entire operation failure, so 
            # end the loop 
            shutdown()
            failure=True
            break
        # if all jobs completed successfully, return true
        else:
            failure=False
    except KeyboardInterrupt:
        shutdown()
    duration=time.time()-start 
    err="Failed to process %d frames."%(_failed)
    if _failed: log.info(err)
    msg="Created %d frames in %.2f seconds"%(_total, duration)
    log.info(msg)
    return failure




    