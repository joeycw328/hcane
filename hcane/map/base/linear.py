#!/usr/bin/env python3
import numpy as np

from .fov import FOV

# OO linear coordinate transform implementation 
class LinearTransformation(object):
    """
    Apply a coordinate transformation mapping lat/lon to 2-D plane intersecting 
    at arbitrary longitude
    """
    def __init__(self, fov=None):
        """
        Transform and store lattitude and longitude as x,y projections
        """
        # store the field of view
        self.fov=fov if isinstance(fov, FOV) else FOV()
        
    def apply(self, lattitude, longitude):
        """
        Transform and store lattitude and longitude as stretched/skewed x,y 
        projections
        """
        # get pixels per degree longitude
        # Note: fov max should be degrees lon between rightmost and leftmost 
        # visible longitude lines. scale should be pixels between righmost and
        # leftmost visible longitude 
        px_per_deg_lon=self.fov.scale/self.fov.max_degree

        # convert inputed longitude to x-pixels from the meridian
        x_px=(longitude-self.fov.meridian)*px_per_deg_lon

        # convert inputed lattitude to pixels from 0 N using transform params
        y_px=(px_per_deg_lon*self.fov.oblation)*lattitude

        return int(x_px),int(y_px)

# if __name__=="__main__":
#     import matplotlib
#     import matplotlib.pyplot as plt
#     # create matplotlib plot
#     fig, ax = plt.subplots()
#     ax.set_aspect("equal")
#     ax.set(xlabel='x', ylabel='y', title='Coord Mapping')
#     # plot lattitudes
#     THETAS = PHIS = np.arange(0.0, FOV.FOV_MAX, 5.0)
#     for phi in PHIS:
#         x_data=[]; y_data=[]
#         for theta in THETAS:
#             trans=GeodesicTransformation(theta, phi)
#             x_data.append(trans.x)
#             y_data.append(trans.y)
#         ax.plot(x_data, y_data)
#     # plot longitudes
#     for theta in THETAS:
#         x_data=[]; y_data=[]
#         for phi in PHIS:
#             trans=GeodesicTransformation(theta, phi)
#             x_data.append(trans.x)
#             y_data.append(trans.y)
#         ax.plot(x_data, y_data)
    
#     fig.savefig("coords_geo4.png", transparent=True, dpi=300)
#     # plt.show()