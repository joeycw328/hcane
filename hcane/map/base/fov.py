#!/usr/bin/env python3
import numpy as np


class FOV(object):
    """
    Abstraction for the properties of the field of view of an observer
    """
    FOV_MAX=85. 
    FOV_MAX_RAD=FOV_MAX*(np.pi/180.)
    # calculate a phase shift to impart for the geodesic transformation
    PHASE_SHIFT=np.pi/(90./(90.-FOV_MAX))
    # Empirically derived factor of oblation, since earth isn't round (from GOES imagery)
    OBLATION=1400./1409.
    
    def __init__(self, 
        max_degree=FOV_MAX, max_rad=FOV_MAX_RAD,
        phase_shift=PHASE_SHIFT, oblation_factor=OBLATION
        ):
        self.__max_degree     = max_degree
        self.__max_rad        = max_rad
        self.__phase_shift    = phase_shift
        self.__oblation       = oblation_factor
        
        self.__scale        = 1.0 
        self.__meridian     = 0.0 

    scale          = property(lambda self: self.__scale)
    meridian       = property(lambda self: self.__meridian)

    max_degree     = property(lambda self: self.__max_degree)
    max_rad        = property(lambda self: self.__max_rad)
    phase_shift    = property(lambda self: self.__phase_shift)
    oblation       = property(lambda self: self.__oblation)
        
    def set(self, scale=None, meridian=None, oblation=None, max_degree=None):
        """
        Set the scale or meridian
        """
        if scale!=None: self.__scale=scale 
        if meridian!=None: self.__meridian=meridian 
        if oblation!=None: self.__oblation=oblation 
        if max_degree!=None: self.__max_degree=max_degree 
