#!/usr/bin/env python3
from .fov import FOV, np

# OO geodesic coordinate transform implementation 
class GeodesicTransformation(object):
    """
    Apply a coordinate transformation mapping lat/lon to 2-D plane intersecting 
    at arbitrary longitude
    """
    def __init__(self, fov=None):
        """
        Transform and store lattitude and longitude as x,y projections
        """
        # store the field of view
        self.fov=fov if isinstance(fov, FOV) else FOV()
        
    def apply(self, lattitude, longitude):
        """
        Transform and store lattitude and longitude as x,y projections
        """
        # store the field of view
        unit_length=1.
        longitude=longitude-self.fov.meridian
        # convert lat/lon to radians
        lon=longitude=(longitude/180)*np.pi 
        lat=lattitude=(lattitude/180)*np.pi 
        # compensate for observer
        geo_lat=self.__geodesic(lat)
        geo_lon=self.__geodesic(lon)
        # calculate y projection 
        yshift=np.sin(lat)*self.fov.phase_shift*np.cos(geo_lon)
        y=unit_length*np.sin(lattitude+yshift)
        # calculate x projection
        xshift=np.sin(lon)*self.fov.phase_shift*np.cos(geo_lat)
        x=np.sin(longitude+xshift)*np.cos(lattitude)
        # compensate for the fact that earth isn't entirely spherical
        y*=self.fov.oblation
        # return x and y scaled to the field of view
        return x*self.fov.scale, y*self.fov.scale
    
    def __geodesic(self, rad_lat_or_long):
        """
        Apply a geodesic transformation to the lat/lon to map from FOV to a full
        disc
        """
        return (rad_lat_or_long/self.fov.max_rad)*(np.pi/2)



if __name__=="__main__":
    import matplotlib
    import matplotlib.pyplot as plt
    # create matplotlib plot
    fig, ax = plt.subplots()
    ax.set_aspect("equal")
    ax.set(xlabel='x', ylabel='y', title='Coord Mapping')
    # plot lattitudes
    THETAS = PHIS = np.arange(0.0, FOV.FOV_MAX, 5.0)
    for phi in PHIS:
        x_data=[]; y_data=[]
        for theta in THETAS:
            trans=GeodesicTransformation(theta, phi)
            x_data.append(trans.x)
            y_data.append(trans.y)
        ax.plot(x_data, y_data)
    # plot longitudes
    for theta in THETAS:
        x_data=[]; y_data=[]
        for phi in PHIS:
            trans=GeodesicTransformation(theta, phi)
            x_data.append(trans.x)
            y_data.append(trans.y)
        ax.plot(x_data, y_data)
    
    fig.savefig("coords_geo4.png", transparent=True, dpi=300)
    # plt.show()