from .base.fov import FOV,np
from .base.geodesic import GeodesicTransformation
from .base.linear import LinearTransformation

TF_GEODESIC  = "geodesic"
TF_LINEAR    = "linear"

class BiasedFOV(FOV):
    """
    Subclass to store parameters specific to goes imagery
    """
    def __init__(self, img_width, img_height, origin=(0,0)):
        # init superclass
        super(BiasedFOV, self).__init__()
        # meridian defaults to -75 for 75W, the GOES16 orbital slot
        self.__img_width=img_width
        self.__img_height=img_height
        self.set(meridian=-75)
        self.__x_bias,self.__y_bias=origin

    x_bias=property(lambda self: self.__x_bias)
    y_bias=property(lambda self: self.__y_bias)
    x_max=property(lambda self: self.__img_width)
    y_max=property(lambda self: self.__img_height)
    
    center=property(lambda self: (self.x_max/2,self.y_max/2))

    def set(self, origin=(None, None), **kwargs):
        super(BiasedFOV,self).set(**kwargs)
        x,y=origin
        if x!=None: self.__x_bias=x 
        if y!=None: self.__y_bias=y 
    
    def to_cartesian(self, x, y):
        """
        Convert x,y from pil coordinates to cartesian coordinates centered at origin
        """
        x_cart=x-self.__x_bias 
        y_cart=self.__y_bias-y
        return x_cart, y_cart

    def to_pil(self, x, y):
        """
        Convert x,y from cartesian coords centered at origin to pil coordinates 
        """
        x_pil=x+self.__x_bias 
        y_pil=self.__y_bias-y
        return x_pil, y_pil


class BiasedTransformation(object):
    """
    Base class to remap the (cartesian) coordinates provided by the base class to be 
    suitable with PIL image, specifically with respect to PIL's coordinate 
    system.
    """
    def __init__(self, fov, ttype):
        if not isinstance(fov, BiasedFOV):
            raise TypeError("parameter fov expected BiasedFOV or subclass thereof")
        if ttype==TF_LINEAR: transform_class=LinearTransformation
        elif ttype==TF_GEODESIC: transform_class=GeodesicTransformation
        else: raise ValueError("Unrecognized transformation parameter: %s"%str(ttype))
        self.__transformation=transform_class(fov)
        self.fov=self.__transformation.fov

    def apply(self, lat, lon):
        """
        Apply the underlying transformation and return pil-biased x,y coordinates
        """
        # get cartesian x and y (centered on lat/lon origin) from utility function 
        raw_x, raw_y=self.__transformation.apply(lat, lon)
        # convert them to pil coordinates
        biased_x,biased_y=self.fov.to_pil(raw_x,raw_y)
        # do bounds checking and return
        biased_x=min(biased_x, self.fov.x_max)
        biased_x=max(0,biased_x)
        biased_y=min(biased_y, self.fov.y_max)
        biased_y=max(0,biased_y)
        return  biased_x, biased_y

class BiasedGeodesicTransformation(BiasedTransformation):
    """
    Base class to remap the (cartesian) coordinates provided by a geodesic 
    transformation to be suitable with PIL image, specifically with respect to 
    PIL's coordinate system.
    """
    def __init__(self, fov):
        super(BiasedGeodesicTransformation, self).__init__(fov,TF_GEODESIC)


class BiasedLinearTransformation(BiasedTransformation):
    """
    Base class to remap the (cartesian) coordinates provided by a linear 
    transformation to be suitable with a PIL image, specifically with respect to 
    PIL's coordinate system.
    """
    def __init__(self, fov):
        super(BiasedLinearTransformation, self).__init__(fov,TF_LINEAR)