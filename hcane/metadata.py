#!/usr/bin/env python
"""
Imagery aggregation engine

Multiprocessing engine for scraping imagery from NESDIS server. Wraps the FTP client
implementation in hcane.client to provide parallelization, disk IO, and frame 
processing preparation

@file hcane/hcane.py
@author Josef C. Willhite
@date 15 July 2019
"""
import os
import time
from os import symlink
from os.path import join, exists
from ftplib import FTP
import logging
from contextlib import contextmanager
from multiprocessing.pool import Pool, ThreadPool 
import signal 
import traceback as tb

import threading

from .eyelevel import eyeinfo
from .client import NHCMetadataClient as Client
from . import runtime

ADV_SUBDIR          = "adv"
LOG_LEVEL	        = logging.INFO
THREADS_PER_PROC    = 10
# setup logging
log=None 

center_data=[]
synchronization=threading.Lock()

# setup parallelization 
def worker_setup():
    this_thread.client=Client
    log.debug("Created connection to server: %s"%str(this_thread.client))

pool=None
this_thread=threading.local()
CONTINUE=True
received=0
total=0
def job(adv_idx, destdir):
    if not CONTINUE: return
    global received
    global total
    # get the name and path on-disk where to place the image
    adv_type,adv_id=advisories[adv_idx]
    adv_path=join(destdir, adv_id)
    raw_adv=""
    # no point double-downloading advisories that already exist
    if exists(adv_path):
        log.info("[%04d/%04d] Skipping Advisory %s: already exists! "%(adv_idx,total,adv_id))
        return
    # here we scrape the advisory
    else:
        try:
            # try to get the advisory using this thread's client
            log.info("[%04d/%04d] Scraping advisory: %s"%(adv_idx,total,adv_id))
            # raw_adv=this_thread.client.get_advisory(adv_id)
            raw_adv=this_thread.client.get_advisory(adv_id,adv_type)
        except EOFError:
            # if the connection is dropped, reconnect and retry
            log.warn("[%04d/%04d] Server closed connection. Retrying.."%(adv_idx,total))
            this_thread.client.connect()
            # raw_adv=this_thread.client.get_advisory(adv_id)
            raw_adv=this_thread.client.get_advisory(adv_id,adv_type)
        # data come as binary text. decode and dump them to disk
        raw_adv=raw_adv.decode("utf-8")
        with open(adv_path, "w+") as advisory:
            log.info("[%04d/%04d] Dumping advisory cache: %s"%(adv_idx,total,adv_id))
            advisory.write(raw_adv)
        received+=1

    log.info("[%04d/%04d] Finding storm center..."%(adv_idx,total))
    advisory=raw_adv.strip().split('\n')
    date,lat,lon=eyeinfo.parse_advisory(advisory,adv_type)
    if not (date and lat and lon):
        log.warning("[%04d/%04d] Couldn't find storm center in advisory: %s "%(adv_idx,total,adv_id))
        return
    log.info("[%04d/%04d] Center located near (%f,%f) at %s."%(adv_idx,total,float(lat),float(lon),date))
    with synchronization:
        center_data.append((date,lat,lon))
    # symlink the newly received advisory into the frame processing queue so the 
    # framed processing daemon can deal with them
    # log.debug("proceeding to requeue...")
    # link_name=LINK_TEXT%(HCANE_NAME, adv_idx)
    # link_dir=join(destdir,QUEUE_SUBDIR)
    # link_path=join(link_dir, link_name)
    # if exists(link_path): 
    #     log.debug("Link %s already exists"%link_path)
    #     link_end=os.readlink(link_path)
    #     link_end=join(link_dir, link_end)
    #     if not exists(link_end):
    #         log.warn("Link %s is stale. Removing."%link_path)
    #         os.remove(link_path)
    #     else:
    #         log.debug("Skipping exising link.")
    #         return
    # img_relpath=join("..",adv_id)
    # log.debug("[%04d/%04d] Linking %s --> %s"%(adv_idx,total,adv_path, link_path))
    # os.symlink(img_relpath, link_path)


class JobFailureError(Exception):
    """Error raised for failed jobs"""


def scrape_advisories(ADV_DIR, advisories):
    global CONTINUE
    global received
    global log
    global total
    HCANE_NAME=runtime.args.metadata.NAME
    log=logging.getLogger("hcane.%s.md"%HCANE_NAME)
    log.setLevel(LOG_LEVEL)
    # start the worker pool
    pool=ThreadPool(THREADS_PER_PROC, worker_setup)

    # scrape data 
    log.info("Discovered %d advisories..."%len(advisories))
    total=len(advisories)
    received=0
    all_results=[]
    for i in range(total):
        def try_job(idx):
            try: job(idx,ADV_DIR)
            except: return tb.format_exc()
        
        all_results.append(pool.apply_async(try_job,(i,)))

    pool.close()
    exception=None
    # process results
    try:
        for job_result in all_results:
            res=job_result.get()
            if res: 
                CONTINUE=False; exception=res
                raise JobFailureError()
    except KeyboardInterrupt:
        log.warn("Operation aborted!")
        log.info("Terminating worker threads")
        pool.terminate()
    except JobFailureError:
        log.error("A job has failed!!")
        log.info("Terminating worker threads")
        pool.terminate()
    except:
        log.error("Unknown error")
        log.info("Terminating worker threads")
        pool.terminate()
        exception=traceback.format_exc()
    log.warn("Waiting for threads shutdown...")
    pool.join()
    return exception

# def parallel_job(*args):
#     try: return scrape_advisories(*args)
#     except: return tb.format_exc()

def list_advisories():
    # get the image list from nesdis
    log.info("Getting advisory list from NHC/NOAA server: %s"%Client.URL_BASE)
    if not runtime.args.metadata.ID:
        log.warning("Storm config has empty metadata.ID field. Cannot retrieve advisory list.")
        return None
    advisories=Client.list_advisories(runtime.args.metadata.ID)
    # log.info("Success.")
    # min_start_date=int(args.source.STARTDATE)
    # log.info("Pruning imagery from before: %d"%min_start_date)
    # time.sleep(3)
    # # prune old imagery by start date
    # advisories_to_keep=[]
    # for advisories in advisories:
    #     try: img_date=int(advisories[:7])
    #     except: continue 
    #     if img_date < min_start_date: 
    #         log.info("Skipping old image: %s"%advisories)
    #         continue 
    #     advisories_to_keep.append(advisories)
    return sorted(advisories)

def run():
    # global pool
    global advisories
    global received
    global log
    global total
    HCANE_NAME=runtime.args.metadata.NAME
    # global TARGET_FTP_DIR
    # global IMG_RESOLUTION
    # global FORCE_REQUEUE
    log=logging.getLogger("hcane.%s.md"%HCANE_NAME)
    log.setLevel(LOG_LEVEL)
    # TARGET_FTP_DIR=server.sectors.get(args.source.SECTOR)
    # log.debug(args.image.SOURCE_RES)
    # IMG_RESOLUTION="%dx%d"%args.image.SOURCE_RES
    # FORCE_REQUEUE=args.image.REQUEUE or args.image.Q_ONLY
    # create the directory in which to store raw data
    storm_data_dir=join(runtime.args.daemon.DATADIR, HCANE_NAME)
    # HCANE_DATA_DIR=join(raw_data_dir, HCANE_NAME)
    # if not exists(HCANE_DATA_DIR):
    #     log.warn("Storm directory doesn't exist: %s"%HCANE_DATA_DIR)
    #     log.info("Creating storm directory: %s"%HCANE_DATA_DIR)
    #     os.makedirs(HCANE_DATA_DIR)
    ADV_DIR=join(storm_data_dir,ADV_SUBDIR)
    log.info("Placing advisories in directory: %s"%ADV_DIR)
    if not exists(ADV_DIR):
        log.warn("Advisory directory doesn't exist: %s"%ADV_DIR)
        log.info("Creating advisory directory: %s"%ADV_DIR)
        os.makedirs(ADV_DIR)
    # frame_dir=join(HCANE_DATA_DIR,FRAME_SUBDIR)
    # if not exists(frame_dir):
    #     log.warn("Frame directory doesn't exist: %s"%frame_dir)
    #     log.info("Creating frame directory: %s"%frame_dir)
    #     os.makedirs(frame_dir)
    # get images from nesdis 
    # if args.image.Q_ONLY:
    #     log.warn("Skipping download. Refreshing framer queue.")
    #     hi_def=sorted([i for i in os.listdir(HCANE_DATA_DIR) if ".jpg" in i])
    # else:
    start=time.time()
    log.info("Running discovery...")
    advisories=list_advisories()
    if advisories==None:
        return True
    # create a few process to do work
    # all_results=[]
    # pool=ThreadPool(2)
    # mid_idx=int(len(advisories)/2)-1
    # proc_args=(raw_data_dir, advisories, 0)
    # all_results.append(pool.apply_async(parallel_job,proc_args))
    # proc_args=(raw_data_dir, advisories, mid_idx+1)
    # all_results.append(pool.apply_async(parallel_job,proc_args))
    result=scrape_advisories(ADV_DIR, advisories)
    if result:
        for line in result.split('\n'):
            log.critical(line)
        return True
    # on success, dump new eyeinfo
    if center_data:
        extant=set()
        new_eyeinfo=[]
        center_data.sort()
        for info in center_data:
            if info[0] in extant: continue
            new_eyeinfo.append(info);extant.add(info[0])
        log.info("Found storm center data in %d new advisories."%len(new_eyeinfo))
        log.info("Updating storm state data...")
        eyeinfo.write_state_data(*[eyeinfo.EyeState(*data) for data in new_eyeinfo])
    duration=time.time()-start 
    log.info("Storm metadata refresh completed in %.2f seconds"%duration)
    # pool.close()
    return False