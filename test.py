#!/usr/bin/env python3
"""
Programatic entrypoint into the test infrastructure 

Remote debuggers can use this file as an entrypoint into the 
nose testing infrastructure. Specify this script as the script 
to debug, and it will allow your debugger to execute the 
nose testing infrastructure (i.e. the "make check" equivalent)
and step through all of the included tests 

"""

import nose 

nose.main()