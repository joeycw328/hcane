# hcane

__hcane__ is a python package that provides utilities for stitching raw spacecraft imagery into moving earthscapes

It also provides a systemd service, `hcaned`, that uses these utilities to conjure high-definition video from raw spacecraft imagery provided by the GOES-EAST satellite. 

---

## hcaned
`hcaned` is not actually a single systemd service, but instead comprises 5 systemd [unit templates](https://www.freedesktop.org/software/systemd/man/systemd.unit.html) that work together to provide it's core capabilities:

- `hcaned-ftpd@.service`: *oneshot* service that reads storm configuration (NESDIS server, image processing parameters, etc.) from a persistent *.ini* file on disk and uses the information therein to aggregate raw high-def (4000x4000px) image frames from NESDIS [public FTP server](https://www.star.nesdis.noaa.gov/GOES/index.php) (click any sector to reveal links for FTP access) and stores them on-disk for processing
- `hcaned-framerd@.service`: *simple* service that runs after `hcaned-ftpd@` exits which processes the raw frames that it aggregated. Frames are cropped to 16:9 (4000x2250), and edited to include image credit and date/time information
- `hcaned-refreshed@.target`: aggregate target used to initiate the above named services in appropriate order 
- `hcaned@.timer`: a systemd timer that runs the ftp and framer services periodically (default: every hour)
- `hcaned@.service`: *oneshot* metaservice that starts and stops the timer. It sets *RemainAfterExit=yes*, which allows the service to appear in the systemd service list.

To start the service for a specific storm, first create a storm-configuration file with the desired *name* (i.e. *barry.ini*) in the */var/lib/hcaned/storms* directory. Many such files can be created.

Then, start the `hcaned` service with:

`sudo systemctl start hcaned@<name>`

By default `hcaned` will aggregate raw data and processed frames for the specified storm to the */var/lib/hcaned/data/<name>* directory. This allows multiple instances of `hcaned` to run side-by-side, allowing the user to monitor multiple storms simultaneously.


## Installation
Installation can be performed using [pip](https://pypi.org/project/pip/).

First, get the source:

`$ git clone https://joeycw328@bitbucket.org/joeycw328/hcane.git`

Then install with `pip`:

`$ cd hcane`

`$ sudo pip install .`

Uninstall with `pip` as well: 

`$ sudo pip uninstall hcaned`

## License 
[MIT License](https://choosealicense.com/licenses/mit/)

© 2019 Josef C. Willhite