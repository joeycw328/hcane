import nose 
from nose.tools import * 

from hcane.basic.key import KeyManager, Key 

def test_metaclass():
    "[KEYCONTAINER] Test metaclass key container creation"
    class tkey(KeyManager):
        __tst=None
        GULF_OF_MEXICO  = Key("pub/star/goes/GOES16/ABI/SECTOR/gm/GEOCOLOR/")
        CONUS           = Key("pub/star/goes/GOES16/ABI/SECTOR/CONUS/GEOCOLOR/")
        def tst(): pass
    keys=tkey.all()
    assert_true(tkey.CONUS)
    assert_is_instance(tkey.CONUS, str)
    
    class tkey2(KeyManager):
        __tst=None
        BS  = Key("pub/star/goes/GOES16/ABI/SECTOR/gm/GEOCOLOR/")
        NOT_MEXICO           = Key("pub/star/goes/GOES16/ABI/SECTOR/CONUS/GEOCOLOR/")
    keys=tkey2.all()
    assert_not_in("CONUS",keys)    
