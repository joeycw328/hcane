__test__=True
from PIL import Image, ImageFont, ImageDraw 
import numpy as np
from hcane.map.transform import BiasedGeodesicTransformation
from hcane.map.transform import BiasedFOV

IMG="test/test-7200x4320.jpg"
ORIGIN=(1824,3870)
TRANSFORM_SCALE=5444

def test_geodesic_taw():
    """[GEODESIC] Test geodesic mapping accuracy (taw)"""
    # open the image
    og_img=Image.open(IMG)
    img=og_img.copy()
    # draw all the longitude lines
    drawer=ImageDraw.Draw(img)
    LONS = np.arange(-100.0, 10.0, 0.5)
    LATS = np.arange(-10, 45.0, 5.0)
    # add longitute dots
    fov=BiasedFOV(img_width=7200, img_height=4320, origin=ORIGIN)
    fov.set(scale=TRANSFORM_SCALE)
    transform=BiasedGeodesicTransformation(fov)
    for lon in LONS:
        x_data=[]; y_data=[]
        for lat in LATS:
            # trans=BGT(lon, lat, fov)
            # x,y=trans.x,trans.y
            x,y=transform.apply(lat, lon)
            circ=(x-4,y-4,x+4,y+4)
            drawer.ellipse(circ, fill="yellow",outline="yellow")
    img.show()
    # img.save("taw_frame.jpg")

def test_geodesic_eus():
    """[GEODESIC] Test geodesic mapping accuracy (eus)"""
    IMG="test/test-eus-4000x4000.jpg"
    ORIGIN=(1820,9040)
    TRANSFORM_SCALE=10888
    # open the image
    og_img=Image.open(IMG)
    img=og_img.copy()
    # draw all the longitude lines
    drawer=ImageDraw.Draw(img)
    LONS = np.arange(-85.0, -55.0, 0.5)
    LATS = np.arange(20, 50.0, 5.0)
    # add longitute dots
    fov=BiasedFOV(img_width=4000, img_height=4000, origin=ORIGIN)
    fov.set(scale=TRANSFORM_SCALE)
    transform=BiasedGeodesicTransformation(fov)
    for lon in LONS:
        x_data=[]; y_data=[]
        for lat in LATS:
            # trans=BGT(lon, lat, fov)
            # x,y=trans.x,trans.y
            x,y=transform.apply(lat, lon)
            circ=(x-4,y-4,x+4,y+4)
            drawer.ellipse(circ, fill="yellow",outline="yellow")
    img.show()
    # img.save("taw_frame.jpg")

def test_geodesic_gm():
    """[GEODESIC] Test geodesic mapping accuracy (gm)"""
    IMG="test/test-gm-4000x4000.jpg"
    ORIGIN=(4696,7350)
    TRANSFORM_SCALE=10888
    # open the image
    og_img=Image.open(IMG)
    img=og_img.copy()
    # draw all the longitude lines
    drawer=ImageDraw.Draw(img)
    LONS = np.arange(-100.0, -75.0, 0.5)
    LATS = np.arange(15, 40.0, 5.0)
    # add longitute dots
    fov=BiasedFOV(img_width=4000, img_height=4000, origin=ORIGIN)
    fov.set(scale=TRANSFORM_SCALE)
    transform=BiasedGeodesicTransformation(fov)
    for lon in LONS:
        x_data=[]; y_data=[]
        for lat in LATS:
            # trans=BGT(lon, lat, fov)
            # x,y=trans.x,trans.y
            x,y=transform.apply(lat, lon)
            circ=(x-4,y-4,x+4,y+4)
            drawer.ellipse(circ, fill="yellow",outline="yellow")
    img.show()
    # img.save("taw_frame.jpg")

