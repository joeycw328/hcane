import requests 
import datetime
import re
from os.path import exists,join,dirname

URL="https://www.nhc.noaa.gov/archive/2019/al13/al132019.fstadv.%03d.shtml?text"

NUM_ADVISORIES=41 

OUT_NAME="advisory_%d.txt"

START_STRING="<div class='textproduct'>"
END_STRING="</pre>"

THIS_DIR=dirname(__file__)

def extract_text(raw_html_lines):
    "Scrape text product from html advisory page"
    text_prod=""
    # fast forward to the text product 
    idx=0
    line=raw_html_lines[0]
    while START_STRING not in line: 
        idx+=1; line=raw_html_lines[idx]
    # scrape out all the text lines 
    idx+=1; line=raw_html_lines[idx]
    while END_STRING not in line: 
        text_prod+='\n'
        text_prod+=line
        idx+=1; line=raw_html_lines[idx]
    # return the text product
    return text_prod

def parse_coords(lat, lon):
    """Convert from N/S, E/W to +/-"""
    is_negative=lat[-1]=='S'
    lat_=float(lat[:-1])*(1-2*is_negative)
    is_negative=lon[-1]=='W'
    lon_=float(lon[:-1])*(1-2*is_negative)
    return lat_,lon_

TIME_FORMAT="%H%M %Z %a %b %d %Y"
INTEGER_FORMAT="%Y%j%H%M"
LOC_RE_TXT="CENTER LOCATED NEAR ([0-9]+\.[0-9][NS] +[0-9]+\.[0-9][EW])"
LOC_RE=re.compile(LOC_RE_TXT)
def parse_text(advisory_lines):
    date=lat=lon=None
    # fast forward to the date line and extract it
    lines=(line for line in advisory_lines)
    line=lines.next().strip()
    while True: 
        try:
            dt=datetime.datetime.strptime(line,TIME_FORMAT)
            date=dt.strftime(INTEGER_FORMAT)
            break 
        except:
            try: line=lines.next().strip()
            except: break
    # fast forward to the date coords line and extract it
    line=lines.next().strip()
    while True: 
        match=LOC_RE.search(line)
        if match:
            res=match.groups()[0].split()
            lat,lon=parse_coords(*res)
            break 
        else:
            try: line=lines.next().strip()
            except: break
    return date,lat,lon

if __name__=="__main__":
    for i in range(NUM_ADVISORIES):
        idx=i+1
        outfile=OUT_NAME%idx
        if exists(join(THIS_DIR,outfile)): 
            print "Advisory %d exists. Skipping scrape"%idx
            continue
        url=URL%idx
        print "Scraping advisory %d."%idx
        resp=requests.get(url)
        with open(outfile, 'w') as output:
            txtprod=extract_text(resp.content.strip().split('\n'))
            output.write(txtprod)
    for i in range(NUM_ADVISORIES):
        idx=i+1
        outfile=OUT_NAME%idx
        with open(outfile,'r') as adv_file:
            txtprod=adv_file.read()
            dt=parse_text(txtprod.strip().split('\n'))
        print "%s,%s,%s"%dt