__test__=False
import nose 
from nose.tools import * 

from hcane.config import wizard 
from hcane.main import cline


def test_wizard():
    args=cline.parse_args()
    wizard.run(args)
