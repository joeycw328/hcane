from nose.tools import *
import os
from os.path import dirname,join,exists
import shutil 
import datetime

from hcane.runtime import args 
from hcane.eyelevel import state 

from . import setUpModule, tearDownModule, EYE_INFO
from . import TEST_STORM_NAME, TEST_DATA_DIR

BROKEN_INFO=join(TEST_DATA_DIR,TEST_STORM_NAME,"broken.eyeinfo")

def broken_setup():
    """Swap good eyeinfo file for broken one"""
    backup='.'.join([EYE_INFO,"bk"])
    shutil.move(EYE_INFO, backup)
    shutil.copy(BROKEN_INFO, EYE_INFO)

def broken_teardown():
    """Swap broken eyeinfo file for good one"""
    backup='.'.join([EYE_INFO,"bk"])
    shutil.move(backup, EYE_INFO)

def empty_setup():
    """Move eyeinfo file"""
    backup='.'.join([EYE_INFO,"bk"])
    shutil.move(EYE_INFO, backup)

def empty_teardown():
    """Restore eyeinfo file"""
    backup='.'.join([EYE_INFO,"bk"])
    shutil.move(backup, EYE_INFO)


# helper functions 
def get_md_rows():
    """
    Get intified lists of metadata rows 
    """
    rows=[]
    # open reader and intify each row
    with state.metadata_reader() as reader:
        for row in reader:
            rows.append([float(i) for i in row])
    # ensure there were rows returned
    err="Reader returned no rows."
    assert_true(len(rows), err) 
    return rows

def get_md_states():
    """
    Get intified lists of metadata rows 
    """
    # load states from api function
    states=state.load_state_data()
    # ensure there were states returned
    err="State loader returned no states."
    assert_true(len(states), err) 
    return states

# test functions
@with_setup(broken_setup,broken_teardown)
def test_reader_malformed():
    """[METADATA] Test reader throws on malformed mdfile (CONTROL)"""
    with assert_raises(state.MalformedMetadataError):
        get_md_rows() 

def test_reader_return_types():
    """[METADATA] Test reader returns well-formed rows (CONTROL)"""
    get_md_rows()

def test_load_function_call():
    """[METADATA] Test load_state function call (CONTROL)"""
    get_md_states()
    
def test_load_state_return_types():
    """[METADATA] Test load_state function call return types """
    # get raw csv rows
    states=get_md_states()
    for eyestate in states:
        assert_is_instance(eyestate, state.EyeState)
    
def test_load_state_accuracy():
    """[METADATA] Test load_state data accuracy"""
    # get raw csv rows
    rows=get_md_rows()
    states=get_md_states()
    err="Found %s for eyestate %s, expected %s"
    for i in range(len(rows)):
        # get the data from the md reader
        dt,lat,lon=rows[i]
        # match it to the eyestate values 
        state=states[i]
        msg=err%(str(dt),"datetime",str(state.int))
        assert_equal(dt,state.int, msg)
        msg=err%(str(lat),"lat",str(state.lat))
        assert_equal(lat,state.lat, msg)
        msg=err%(str(lon),"lon",str(state.lon))
        assert_equal(lon,state.lon, msg)


@with_setup(empty_setup,empty_teardown)
def test_writer_create():
    """[METADATA] Test writer creates nonexistent file (CONTROL)"""
    # write some state data
    today=datetime.datetime.today().strftime(state.EyeState.DATEFORMAT)
    eyestate=state.EyeState(today,0,0)
    state.write_state_data(eyestate)
    # check if the file exists
    err="No eyeinfo file found"
    assert_true(exists(EYE_INFO), err)

@with_setup(empty_setup,empty_teardown)
def test_writer_create_validity():
    """[METADATA] Test writer creates valid file (CONTROL)"""
    # write some state data
    today=datetime.datetime.today().strftime(state.EyeState.DATEFORMAT)
    eyestate=state.EyeState(today,0,0)
    state.write_state_data(eyestate)
    # see if the data load successfully
    get_md_states()

@with_setup(broken_setup,broken_teardown)
def test_writer_malformed():
    """[METADATA] Test writer chokes on malformed file"""
    # write some state data
    today=datetime.datetime.today().strftime(state.EyeState.DATEFORMAT)
    eyestate=state.EyeState(today,0,0)
    with assert_raises(state.MalformedMetadataError):
        state.write_state_data(eyestate)

def test_writer_append():
    """[METADATA] Test writer appends to extant file"""
    # get the state data 
    states_before=get_md_states()
    # write some state data
    states_to_add=5
    eyestates=[]
    today="20192750000"
    for i in range(states_to_add):
        amend=i*5 
        dt=str(int(today)+amend)
        eyestate=state.EyeState(dt,amend,amend)
        eyestates.append(eyestate)
        state.write_state_data(eyestate)
    # get new state data 
    states_after=get_md_states()
    # check if all state data were added
    added_states=len(states_after)-len(states_before)
    err="Expected %d new states, found %d"
    assert_equal(added_states, states_to_add,err%(states_to_add,added_states))
    # check for state data validity 
    err="Found %s for added eyestate %s, expected %s"
    for i in range(added_states):
        # get the data from the md reader
        state_control=eyestates[-1-i]
        # match it to the eyestate values 
        state_test=states_after[-1-i]
        msg=err%(str(state_test.int),"datetime",str(state_control.int))
        assert_equal(state_test.int,state_control.int, msg)
        msg=err%(str(state_test.lat),"lat",str(state_control.lat))
        assert_equal(state_test.lat,state_control.lat, msg)
        msg=err%(str(state_test.lon),"lon",str(state_control.lon))
        assert_equal(state_test.lon,state_control.lon, msg)
