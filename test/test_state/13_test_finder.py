from nose.tools import *

from hcane.eyelevel.view import ViewPortFOV
from hcane.eyelevel.finder import EyeFinder

from . import setUpModule, tearDownModule

def test_finder_load():
    """[FINDER] Test EyeFinder load_data call (CONTROL)"""
    fov=ViewPortFOV(0,0,0,0)
    finder=EyeFinder(fov)
    finder.load_data()

def test_finder_bins():
    """[FINDER] Test EyeFinder contains intervals"""
    fov=ViewPortFOV(0,0,0,0)
    finder=EyeFinder(fov)
    finder.load_data()
    intervals=finder.bins 
    assert_true(intervals, "Got no intervals")

