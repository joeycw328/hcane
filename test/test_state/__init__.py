import os
from os.path import dirname,join,exists
import shutil 

from hcane.runtime import args 
from hcane.eyelevel import state 

old_data_dir=None 
old_storm_name=None
TEST_STORM_NAME="stormy"
TEST_DATA_DIR=join(dirname(__file__), "data")

TEMPLATE_INFO=join(TEST_DATA_DIR,TEST_STORM_NAME,"test.eyeinfo")
EYE_INFO=join(TEST_DATA_DIR,TEST_STORM_NAME,state._MDFILE)


# test fixtures
def setUpModule():
    """
    Update runtime args to point to test data directory
    """
    args.image.OFFSET=-300
    args.image.HOFFSET=1300
    global old_data_dir
    global old_storm_name
    # store original configuration 
    old_data_dir=args.daemon.DATADIR
    old_storm_name=args.metadata.NAME
    # update configuration
    args.daemon.DATADIR=TEST_DATA_DIR
    args.metadata.NAME=TEST_STORM_NAME 
    shutil.copy(TEMPLATE_INFO, EYE_INFO)


def tearDownModule():
    """
    Restore runtime args
    """
    args.daemon.DATADIR=old_data_dir
    args.metadata.NAME=old_storm_name
    if exists(EYE_INFO): os.remove(EYE_INFO)
