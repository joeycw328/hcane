import nose 
from nose.tools import * 

from hcane.client import NESDISCDNClient as Client
from hcane.imager import bands, imagers
from hcane.runtime import args 

def test_floater_query():
    "[NESDISCDN] Test floater for 'dorian' is discovered (CONTROL)"
    storm_name="dorian"
    floater_id="AL052019"
    floaters=Client.get_floater_ids(storm_name) 
    err="No floater ids matching query '%s'"%storm_name 
    assert_true(floaters, err)
    err="Expected %d floater id(s), got %d: %s"
    err=err%(1, len(floaters), str(floaters))
    assert_equal(len(floaters), 1, err)
    for (id,name) in floaters:
        err="Expected storm with id %s, got %s"%(floater_id, id)
        assert_equal(floater_id, id, err)

def test_floater_response_type():
    "[NESDISCDN] Test floater query returns strings"
    storm_name="dorian"
    floater_id="AL052019"
    floaters=Client.get_floater_ids(storm_name) 
    err="Expected return type %s for floater %s, got %s"
    for (id,name) in floaters:
        err_msg=err%(str.__name__, "ID", type(id).__name__)
        assert_is_instance(id, str, err_msg)
        err_msg=err%(str.__name__, "name", type(name).__name__)
        assert_is_instance(name, str, err_msg)

def test_floater_url():
    "[NESDISCDN] Test floater data urls are properly constructed"
    floater_id="AL052019"
    floater_band=bands.GEOCOLOR
    exp_url="https://cdn.star.nesdis.noaa.gov/FLOATER/data/AL052019/GEOCOLOR"
    floater_url=Client.floater_data_url(floater_id, bands.GEOCOLOR) 
    err="Expected url %s for floater %s, got %s"
    err_msg=err%(exp_url, floater_id, floater_url)
    assert_equal(exp_url, floater_url, err_msg)
    floater_url=Client.floater_data_url(floater_id, "GEOCOLOR") 
    err="Expected url %s for floater %s, got %s"
    err_msg=err%(exp_url, floater_id, floater_url)
    assert_equal(exp_url, floater_url, err_msg)

def test_sector_url():
    "[NESDISCDN] Test sector data urls are properly constructed"
    band=bands.GEOCOLOR
    sector_id=imagers.GOES16.sectors.GULF_OF_MEXICO
    exp_url="https://cdn.star.nesdis.noaa.gov/GOES16/ABI/SECTOR/gm/GEOCOLOR"
    sector_url=Client.sector_data_url(sector_id, band) 
    err="Expected url %s for sector %s, got %s"
    err_msg=err%(exp_url, sector_id, sector_url)
    assert_equal(exp_url, sector_url, err_msg)
    sector_url=Client.sector_data_url(sector_id, 'GEOCOLOR') 
    err_msg=err%(exp_url, sector_id, sector_url)
    assert_equal(exp_url, sector_url, err_msg)
    
def test_get_list_sector():
    "[NESDISCDN] Test get sector image list"
    args.source.BAND=bands.GEOCOLOR
    args.source.SECTOR=imagers.GOES16.sectors.GULF_OF_MEXICO
    args.source.FLOATER=False
    err="Got empty list"
    assert_true(Client.get_image_list(), err)

def test_get_list_floater():
    "[NESDISCDN] Test get floater image list"
    args.source.FLOATER=True
    args.source.FLOATER_ID="AL052019"
    err="Got empty list"
    assert_true(Client.get_image_list(), err)

def test_get_image_sector():
    "[NESDISCDN] Test get sector image"
    args.source.BAND=bands.GEOCOLOR
    args.source.SECTOR=imagers.GOES16.sectors.GULF_OF_MEXICO
    args.source.FLOATER=False
    img_file=Client.get_image_list()[-1]
    err="Got empty image"
    assert_true(Client.get_image(img_file), err)

def test_get_image_floater():
    "[NESDISCDN] Test get floater image"
    args.source.FLOATER=True
    args.source.FLOATER_ID="AL052019"
    img_file=Client.get_image_list()[-1]
    err="Got empty image"
    assert_true(Client.get_image(img_file), err)
