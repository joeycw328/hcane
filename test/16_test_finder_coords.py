__test__=False
from PIL import Image, ImageFont, ImageDraw 
import numpy as np
from os.path import join
from hcane.map.transform import BiasedGeodesicTransformation
from hcane.map.transform import BiasedFOV
from hcane.imager import imagers

import test_state
from test_state import setUpModule, tearDownModule, TEST_DATA_DIR
from hcane.eyelevel.finder import EyeFinder
from hcane.eyelevel import view 
from hcane.eyelevel.view import EyeViewPort, ViewPortFOV

IMG="test/test-7200x4320.jpg"
ORIGIN=(1824,3852)
TRANSFORM_SCALE=5444
DATEFILE=join(TEST_DATA_DIR, "dates.txt")

def test_geodesic():
    """[EYEFINDER] Test eyefinder coordinate resolution"""
    # open the image
    og_img=Image.open(IMG)
    img=og_img.copy()
    # get all the dates from the test data file
    with open(DATEFILE,'r') as datefile:
        date_strings=datefile.readlines()
    # create geodesic fov
    fov=BiasedFOV(img_width=7200, img_height=4320, origin=ORIGIN)
    fov.set(scale=TRANSFORM_SCALE)
    transform=BiasedGeodesicTransformation(fov)
    drawer=ImageDraw.Draw(img)
    # resolve them to coordinates using an eyefinder
    finder=EyeFinder(fov)
    finder.load_data()
    coords=[coord for (str_,coord) in finder.iter_coordinates(date_strings)]
    # draw the starting x/y
    x,y=transform.apply(*coords[0])
    circ=(x-10,y-10,x+10,y+10)
    drawer.ellipse(circ, fill="red",outline="red")
    # draw the path of the storm
    for (lat,lon) in coords[1:-1]:
        x,y=transform.apply(lat, lon)
        circ=(x-4,y-4,x+4,y+4)
        drawer.ellipse(circ, fill="yellow",outline="yellow")
    # draw the ending lat/lon
    x,y=transform.apply(*coords[-1])
    circ=(x-10,y-10,x+10,y+10)
    drawer.ellipse(circ, fill="blue",outline="blue")
    # show/save the image
    img.show()
    # img.save("taw_frame.jpg")


def test_eyeview():
    """[EYEVIEWPORT] Test EyeViewPort center resolution"""
    # open the image
    og_img=Image.open(IMG)
    img=og_img.copy()
    # create geodesic fov
    fov=ViewPortFOV(img_width=7200, img_height=4320)
    tparams=view.params[imagers.GOES16.sectors.TROPICAL_ATLANTIC]
    fov.set(origin=tparams.origin)
    fov.set(scale=tparams.scale)
    crop_window=EyeViewPort(fov)
    drawer=ImageDraw.Draw(img)
    # get all the dates from the test data file
    with open(DATEFILE,'r') as datefile:
        date_strings=datefile.readlines()
    # resolve them to cartesian coordinates using an eyefinder
    finder=EyeFinder(fov)
    finder.load_data()
    coords=[coord for (str_,coord) in finder.iter_coordinates(date_strings, apply_geodesic=True)]
    # draw the starting x/y
    x,y=crop_window.set_center(*coords[0], apply_geodesic=False)
    circ=(x-10,y-10,x+10,y+10)
    drawer.ellipse(circ, fill="red",outline="red")
    # draw the path of the storm
    for (x,y) in coords[1:-1]:
        x,y=crop_window.set_center(x,y, apply_geodesic=False)
        circ=(x-4,y-4,x+4,y+4)
        drawer.ellipse(circ, fill="yellow",outline="yellow")
    # draw the ending lat/lon
    x,y=crop_window.set_center(*coords[-1], apply_geodesic=False)
    circ=(x-10,y-10,x+10,y+10)
    drawer.ellipse(circ, fill="blue",outline="blue")
    # show/save the image
    img.show()
    # img.save(