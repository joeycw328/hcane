#!/usr/bin/env python
import datetime 

INTEGER_FORMAT="%Y%j%H%M"

START_DATE="20192630750"
END_DATE="20192751500"

if __name__=="__main__":
    dt=datetime.datetime.strptime(START_DATE, INTEGER_FORMAT)
    delta_minutes=0
    as_string=dt.strftime(INTEGER_FORMAT)
    while int(as_string) <= int(END_DATE):
        print as_string 
        delta_minutes+=10
        next_time=dt+datetime.timedelta(minutes=delta_minutes)
        as_string=next_time.strftime(INTEGER_FORMAT)
